import * as firebase from 'firebase';

export class Firebase {
    private static instance = new Firebase();
    private static firebase;

    private constructor() {
        firebase.initializeApp({
            apiKey: "AIzaSyD2MqcRAwglGweXr5iSbmfylEMRfzOhSP0",
            authDomain: "atpmonitor-9e628.firebaseapp.com",
            databaseURL: "https://atpmonitor-9e628.firebaseio.com",
            projectId: "atpmonitor-9e628",
            storageBucket: "atpmonitor-9e628.appspot.com",
            messagingSenderId: "514180937492",
            appId: "1:514180937492:web:d2531df57d29043a73f401"
        });
        Firebase.firebase = firebase;
    }

    public static getInstance(): any {
        return Firebase.firebase;
    }
}
