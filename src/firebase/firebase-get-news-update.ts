import {Firebase} from "./firebase";
import {NewsUpdate} from "../shared/models/news-update";
import {Utils} from "../shared/utils/utils";

export class FirebaseGetNewsUpdate {
    private static readonly TAG_APP = "Big_All_In_One";

    public static getUpdate(callback) {
        let lastTimeStampSeenUpdate = FirebaseGetNewsUpdate.getLastTimestampSeenUpdate();
        let listNewsUpdate = new Array<NewsUpdate>();
        let firebase = Firebase.getInstance();
        let timeString = Utils.formatDate(new Date());
        let referenceNews = firebase.database().ref('NewsUpdate/' + FirebaseGetNewsUpdate.TAG_APP).once('value').then((snapshot) => {
            if (snapshot.exists()) {
                snapshot.forEach((aNewsSnapshot) => {
                    let content = aNewsSnapshot.child('content').val();
                    let timestamp = aNewsSnapshot.child('timestamp').val();
                    let stringTime = FirebaseGetNewsUpdate.getStringTime(new Date(Number(timestamp)));
                    let newsUpdate = new NewsUpdate(content, timestamp, stringTime);
                    if (timestamp > lastTimeStampSeenUpdate) {
                        newsUpdate.seen = false;
                    } else {
                        newsUpdate.seen = true;
                    }
                    listNewsUpdate.push(newsUpdate);
                });
            }
            callback(listNewsUpdate);
        });
    }


    public static saveLastTimestampSeenUpdate(timestamp: number) {
        let userAppDataPath = (<any>window).require('electron').remote.app.getPath('userData');
        let filename = userAppDataPath + '/lastSeenUpdate';
        (<any>window).require('fs').writeFileSync(filename, String(timestamp));
    }

    private static getLastTimestampSeenUpdate(): number {
        try {
            let userAppDataPath = (<any>window).require('electron').remote.app.getPath('userData');
            let filename = userAppDataPath + '/lastSeenUpdate';
            let fs = (<any>window).require('fs');
            if (fs.existsSync(filename)) {
                return Number(fs.readFileSync(filename, 'utf8'));
            } else {
                return 0;
            }
        } catch (e) {
            alert(Utils.getError(e));
        }
        return 0;
    }

    public static getStringTime(date: Date): string { // DD-MM-YYYY HH:mm
        let day = String(date.getDate());
        let month = String((date.getMonth() + 1));
        let year = String(date.getFullYear());
        let hour = String(date.getHours());
        let minute = String(date.getMinutes());
        let second = String(date.getSeconds());
        if (day.length === 1) {
            day = '0' + day;
        }
        if (month.length === 1) {
            month = '0' + month;
        }
        if (hour.length === 1) {
            hour = '0' + hour;
        }
        if (minute.length === 1) {
            minute = '0' + minute;
        }
        if (second.length === 1) {
            second = '0' + second;
        }
        return day + '-' + month + '-' + year + ' ' + hour + ':' + minute;
    }

}
