import {PostGetRequest} from "../shared/request/post-get-request";

export class GetIdMachineForFirebaseMonitor extends PostGetRequest {
    private _idMachine: string = '';

    async task(): Promise<any> {
        /*this.url = 'http://api.db-ip.com/addrinfo?api_key=bc2ab711d740d7cfa6fcb0ca8822cb327e38844f&addr=116.12.250.1';
        await this.sendGet();
        try {
            let result = JSON.parse(this.result);
            let ip = result.address;
            if (ip) {
                this._idMachine = ip;
            } else {
                this._idMachine = this.makeid();
            }
        } catch (e) {
            this._idMachine = this.makeid();
        }*/
        this._idMachine = this.makeid();

    }

    get idMachine(): string {
        return this._idMachine;
    }

    private makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text + new Date().getTime();
    }
}
