import {IpcRenderer} from "electron";

export class MyNotification {
	private data: any;
	private ipc: IpcRenderer;
	private _autoCloseAfterClicked = true;

	set autoCloseAfterClicked(value: boolean) {
		this._autoCloseAfterClicked = value;
	}

	constructor(data: any) {
		this.data = data;
		this.ipc = (<any>window).require("electron").ipcRenderer;
	}

	public show() {
		this.ipc.send("showNoti", this.data);
	}

	public close() {
		this.ipc.send("closeNoti", this.data);
	}

	public onClick(callback) {
		this.ipc.on("clickedNoti", (event, dataNoti) => {
			if (dataNoti.idCampaign === this.data.idCampaign) {
				callback(dataNoti);
				if (this._autoCloseAfterClicked) {
					setTimeout(() => {
						this.close();
					}, 200);
				}

			}
		});
	}
}