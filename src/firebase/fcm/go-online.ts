import {PostGetRequest} from "../../shared/request/post-get-request";

export class GoOnline extends PostGetRequest {
	private nameApp: string;
	private keyOnly: string;
	private data: string;


	constructor(nameApp: string, keyOnly: string, data: string) {
		super();
		this.nameApp = nameApp;
		this.keyOnly = keyOnly;
		this.data = data;
	}

	async task(): Promise<void> {
		this.url = 'http://namlh.me:5430/online';
		await this.sendPost(this.buildParamGonOnline());
	}

	private buildParamGonOnline() {
		let param: any = {};
		param['nameApp'] = this.nameApp;
		param['keyOnly'] = this.keyOnly;
		param['data'] = this.data;
		return param;
	}
}