import {PostGetRequest} from "../../shared/request/post-get-request";

export class ClickedPushNoti extends PostGetRequest {
    private campaignID: string;
    private identifier: string;
    private token: string;


    constructor(campaignID: string, identifier: string, token: string) {
        super();
        this.campaignID = campaignID;
        this.identifier = identifier;
        this.token = token;
    }

    async task(): Promise<any> {
        this.url = 'http://namlh.me:5430/clicked-noti';
        await this.sendPost(this.buildParamSuccessPushNoti());
        console.log('Result send success push noti' + this.result);
    }

    private buildParamSuccessPushNoti() {
        let param: any = {};
        param['token'] = this.token;
        param['identifier'] = this.identifier;
        param['campaign_id'] = this.campaignID;
        return param;
    }
}
