import {
	NOTIFICATION_RECEIVED as ON_NOTIFICATION_RECEIVED,
	NOTIFICATION_SERVICE_ERROR,
	NOTIFICATION_SERVICE_STARTED,
	START_NOTIFICATION_SERVICE,
	TOKEN_UPDATED,
} from 'electron-push-receiver/src/constants';
import {UpdateTokenDevice} from "./update-token-device";
import {Constant} from "../../shared/constrant/Constant";
import {SendSuccessPushNoti} from "./send-success-push-noti";
import {ClickedPushNoti} from "./clicked-push-noti";
import {MyNotification} from "./my-notification";

export class InitFcm {
	private static readonly FCM_SENDERID = '514180937492';
	private static readonly EXPRIED_INFO_VINHVIEN = -1;
	private static readonly EXPRIED_INFO_DUNGTHU = -2;
	private static readonly NAME_APP: string = 'BigAllInOne';


	constructor() {
	}

	public static async init() {
		return new Promise<any>((resolve, reject) => {
			setTimeout(() => {
				if (!Constant.TOKEN_FCM) {
					reject();
				}
			}, 30000);
			const ipcRenderer = (<any>window).require('electron').ipcRenderer;
			// Listen for service successfully started
			ipcRenderer.on(NOTIFICATION_SERVICE_STARTED, async (_, token) => {
				Constant.TOKEN_FCM = token;
				resolve();
				console.log('service successfully started', token);
				const publicIp = (<any>window).require('public-ip');
				let ip = await publicIp.v4();
				let infoExpired = String(this.EXPRIED_INFO_VINHVIEN);
				let identifier = InitFcm.NAME_APP + '-' + ip;
				let updateOrInsertToken = new UpdateTokenDevice(token, identifier, infoExpired, InitFcm.NAME_APP);
				updateOrInsertToken.task();
				console.log('token updated', token)
			});
			// Handle notification errors
			ipcRenderer.on(NOTIFICATION_SERVICE_ERROR, (_, error) => {
				console.log('notification error', error)
			});
			// Send FCM token to backend
			ipcRenderer.on(TOKEN_UPDATED, (_, token) => {
				/*let fullKey = Utils.versionKey;
				let key = fullKey.split(' - ')[0];
				let infoExpired = fullKey.split(' - ')[1];
				let updateOrInsertToken = new UpdateTokenDevice(token, key, infoExpired);
				console.log('token updated', token)*/
			});
			// Display notification
			ipcRenderer.on(ON_NOTIFICATION_RECEIVED, (_, serverNotificationPayload) => {
				if (serverNotificationPayload.notification.body) {
					// payload has a body, so show it to the user
					console.log('display notification', serverNotificationPayload);
					let data = {
						idCampaign: serverNotificationPayload.data.idCampaign,
						icon: serverNotificationPayload.data.imageUrl,
						title: serverNotificationPayload.notification.title,
						body: serverNotificationPayload.notification.body,
						urlOpen: serverNotificationPayload.data.urlOpen
					}
					let noti = new MyNotification(data);
					noti.onClick((dataNoti) => {
						let urlOpen = data.urlOpen;
						(<any>window).require('electron').shell.openExternal(urlOpen);
						let clickedNoti = new ClickedPushNoti(data.idCampaign, '', Constant.TOKEN_FCM);
						clickedNoti.task();
					});
					noti.show();
				} else {
					// payload has no body, so consider it silent (and just consider the data portion)
					console.log('do something with the key/value pairs in the data', serverNotificationPayload.data)
				}
				try {
					let idCampaign = serverNotificationPayload.data.idCampaign;
					let sendSuccessPushNoti = new SendSuccessPushNoti(idCampaign, '', Constant.TOKEN_FCM);
					sendSuccessPushNoti.task();
				} catch (e) {

				}
			});
			// Start service
			ipcRenderer.send(START_NOTIFICATION_SERVICE, InitFcm.FCM_SENDERID);
		});
	}

	private static getExpiredInfoByFullKeyATP(fullKey: string): number {
		let expireInfo = -3; // -3 = NONE
		let infoExpiredFull = fullKey.split(' - ')[1];
		if (infoExpiredFull.includes('Vĩnh viễn')) {
			expireInfo = InitFcm.EXPRIED_INFO_VINHVIEN;
		} else if (infoExpiredFull.includes('Gói dùng thử')) {
			expireInfo = InitFcm.EXPRIED_INFO_DUNGTHU;
		} else {
			expireInfo = Number(infoExpiredFull.split('/')[1].replace(' ngày)', ''));
		}
		return expireInfo;
	}

	private static getKeyByFullKeyATP(fullKey: string): string {
		let key = fullKey.split(' - ')[0];
		return key;
	}
}
