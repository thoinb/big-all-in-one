import {PostGetRequest} from "../../shared/request/post-get-request";

export class GetTimeServer extends PostGetRequest {
	private _timeServer: string = '';

	constructor() {
		super();
	}

	async task(): Promise<void> {
		try {
			this.url = 'http://namlh.me:5430/get-time-server';
			await this.sendGet();
			this._timeServer = JSON.parse(this.result).timeOnServer;
		} catch (e) {
			console.log(e);
		}
	}

	get timeServer(): string {
		return this._timeServer;
	}
}