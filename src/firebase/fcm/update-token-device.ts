import {PostGetRequest} from "../../shared/request/post-get-request";

export class UpdateTokenDevice extends PostGetRequest {
    private token: string;
    private identifier: string;
    private expire_info: string;
    private _expriedAt: number;
    private _note: string = '';
    private nameApp: string;

    constructor(token: string, identifier: string, expire_info: string, nameApp: string) {
        super();
        this.token = token;
        this.identifier = identifier;
        this.expire_info = expire_info;
        this.nameApp = nameApp;
    }


    set note(value: string) {
        this._note = value;
    }

    set expriedAt(value: number) {
        this._expriedAt = value;
    }

    async task(): Promise<any> {
        this.url = 'http://namlh.me:5430/update-or-insert-token';
        await this.sendPost(this.buildParamUpdateOrInsertToken());
        console.log('Result update token' + this.result);
    }


    private buildParamUpdateOrInsertToken() {
        let param: any = {};
        param['token'] = this.token;
        param['identifier'] = this.identifier;
        param['name_app'] = this.nameApp;
        if (this._expriedAt) {
            param['expried_at'] = this._expriedAt;
            param['has_expried_at'] = true;
        }
        param['note'] = this._note;
        param['expire_info'] = this.expire_info;
        return param;
    }
}
