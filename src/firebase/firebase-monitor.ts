import {Firebase} from "./firebase";
import {Utils} from "../shared/utils/utils";
import {GoOnline} from "./fcm/go-online";
import {GetTimeServer} from "./fcm/get-time-server";
import {PostGetRequest} from "../shared/request/post-get-request";

export class FirebaseMonitor {
	private static readonly NAME_APP = "BigAllInOne";

	public static async online(key: string, fcmToken: string) {
		let keyFull = Utils.replaceAll(key, '/', '-');
		let keyAppOnly = keyFull.split(' - ')[0];
		let firebase = Firebase.getInstance();
		let referenceOnline = firebase.database().ref('OnlineDevice/' + FirebaseMonitor.NAME_APP + '/' + keyAppOnly);
		let getTimeServer = new GetTimeServer();
		await getTimeServer.task();
		let timeServer = getTimeServer.timeServer;

		let data = {
			fullKey: keyFull,
			tokenFCM: fcmToken,
			timeOnline: timeServer
		}
		try {
			let postGetRequest = new PostGetRequest();
			postGetRequest.url = 'https://geolocation-db.com/json/697de680-a737-11ea-9820-af05f4014d91';
			await postGetRequest.sendGet();
			let dataIP = JSON.parse(postGetRequest.result);
			for (let key in dataIP) {
				data[key] = dataIP[key];
			}
		} catch (e) {
			console.log(e);
		}
		this.setOnlineToServer(JSON.stringify(data), FirebaseMonitor.NAME_APP, keyAppOnly);
		setInterval(() => {
			this.setOnlineToServer(JSON.stringify(data), FirebaseMonitor.NAME_APP, keyAppOnly);
		}, 120000);

		this.autoDisconnectIfOffline(referenceOnline);
	}

	private static async setOnlineToServer(data: any, nameApp: string, keyOnly: string) {
		try {
			let goOnline = new GoOnline(nameApp, keyOnly, data);
			await goOnline.task();
		} catch (e) {
			console.log(e);
		}
	}


	private static autoDisconnectIfOffline(referenceOnline) {
		referenceOnline.onDisconnect().set(null);
	}
}
