import {EventEmitter, Injectable, Output} from "@angular/core";

@Injectable({
    providedIn: "root"
})
export class EventService {
    @Output() appLstMapProductRenderToButtonAllInOne = new EventEmitter();
    @Output() appProductRenderToButtonAllInOne = new EventEmitter();
    @Output() lstMapProductRenderAllToBigAll = new EventEmitter();
    @Output() lstMapProductRenderAllToBigAllOneElement = new EventEmitter();
    @Output() lstMapProductRenderBigAllToAll = new EventEmitter();
    @Output() scanSoftToButton = new EventEmitter();
    @Output() sendLstProductToLicenseTab = new EventEmitter();
    
    @Output() sendDataCourseTab = new EventEmitter();
    @Output() sendDataFreeCourseTab = new EventEmitter();
    @Output() sendDataPaidCourseTab = new EventEmitter();

    //---------- Doc and book
    // --- Menu Doc and book ==> Parents
    @Output() sendTypeDocAndBook = new EventEmitter();
}
