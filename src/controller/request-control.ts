import {UtilsFormatter} from "../shared/utils/utilsFormatter";
import {Utils} from "../shared/utils/utils";


export class RequestControl {
    private static instance: RequestControl;
    private countRequest: number = 0;

    private constructor() {
    }

    public static getInstance() {
        if (RequestControl.instance == null) {
            RequestControl.instance = new RequestControl();
        }
        return RequestControl.instance;
    }

    public async submitRequestAndSleepIfNeeded() {
        this.countRequest++;
        if (this.countRequest % 10 == 0) {
            await this.sleepRandom(3000, 5000);
            console.log('sleep random activated at count request: ' + this.countRequest);
        }
    }

    public async sleep(ms: number) {
        await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log('sleep for: ' + ms));
    }

    private async sleepRandom(start: number, end: number) {
        let random = Utils.getRandomIntegerInRange(start, end);
        await this.sleep(random);
    }
}
