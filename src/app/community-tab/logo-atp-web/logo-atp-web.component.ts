import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-logo-atp-web',
  templateUrl: './logo-atp-web.component.html',
  styleUrls: ['./logo-atp-web.component.css','./../community-tab.component.css']
})
export class LogoAtpWebComponent implements OnInit {

  @Input() ATP_WEB: any;

  constructor() { }

  ngOnInit() {
  }

}
