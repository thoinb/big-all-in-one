import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-logo-win-erp',
    templateUrl: './logo-win-erp.component.html',
    styleUrls: ['./logo-win-erp.component.css', './../community-tab.component.css']
})
export class LogoWinErpComponent implements OnInit {

    @Input() WINREP: any;

    constructor() {
    }

    ngOnInit() {
    }

}
