import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-logo-voz-vn',
    templateUrl: './logo-voz-vn.component.html',
    styleUrls: ['./logo-voz-vn.component.css','./../community-tab.component.css']
})
export class LogoVozVnComponent implements OnInit {

    @Input() VOZ_VN: any;

    constructor() {
    }

    ngOnInit() {
    }

}
