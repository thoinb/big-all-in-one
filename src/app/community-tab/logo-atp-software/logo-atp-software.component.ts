import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-logo-atp-software',
    templateUrl: './logo-atp-software.component.html',
    styleUrls: ['./logo-atp-software.component.css','./../community-tab.component.css']
})
export class LogoAtpSoftwareComponent implements OnInit {

    @Input() ATP_SOFTWARE: any;

    constructor() { }

    ngOnInit() {
    }

    clickAtpAtpSoftware(url) {
        (<any>window).require('electron').shell.openExternal(url);
    }
}
