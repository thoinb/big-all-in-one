import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-logo-atp-land',
    templateUrl: './logo-atp-land.component.html',
    styleUrls: ['./logo-atp-land.component.css', './../community-tab.component.css']
})
export class LogoAtpLandComponent implements OnInit {

    @Input() ATP_LAND: any;

    constructor() {
    }

    ngOnInit() {
    }

}
