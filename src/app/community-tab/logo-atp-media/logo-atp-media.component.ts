import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-logo-atp-media',
  templateUrl: './logo-atp-media.component.html',
  styleUrls: ['./logo-atp-media.component.css','./../community-tab.component.css']
})
export class LogoAtpMediaComponent implements OnInit {

  @Input() ATP_MEDIA:any;
  constructor() { }

  ngOnInit() {
  }

}
