import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-logo-atp-academy',
    templateUrl: './logo-atp-academy.component.html',
    styleUrls: ['./logo-atp-academy.component.css','./../community-tab.component.css']
})
export class LogoAtpAcademyComponent implements OnInit {

    @Input() ATP_ACADEMY: any;

    constructor() { }

    ngOnInit() {
    }

    clickAtpAcademy(url) {
        (<any>window).require('electron').shell.openExternal(url);
    }

}
