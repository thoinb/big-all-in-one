import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-logo-cv-com-vn',
    templateUrl: './logo-cv-com-vn.component.html',
    styleUrls: ['./logo-cv-com-vn.component.css','./../community-tab.component.css']
})
export class LogoCvComVnComponent implements OnInit {

    @Input() CV_COM_VN: any;

    constructor() {
    }

    ngOnInit() {
    }

}
