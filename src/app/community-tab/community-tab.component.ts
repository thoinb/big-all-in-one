import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {ConstLogo, ConstLogoCode} from "../../shared/const/ConstLogo";
import SendFormContactKh from "../../request/GET/send-form-contact-kh";
import * as moment from 'moment'

declare let $: any;

@Component({
    selector: 'app-community-tab',
    templateUrl: './community-tab.component.html',
    styleUrls: ['./community-tab.component.css']
})
export class CommunityTabComponent implements OnInit ,AfterViewInit{
    @Input() disabled: true;
    private logo: string;

    private ATP_SOFTWARE: any = ConstLogo.get(ConstLogoCode.ATP_SOFTWARE);
    private ATP_ACADEMY: any = ConstLogo.get(ConstLogoCode.ATP_ACADEMY);
    private ATP_WEB: any = ConstLogo.get(ConstLogoCode.ATP_WEB);
    private CV_COM_VN: any = ConstLogo.get(ConstLogoCode.CV_COM_VN);
    private ATP_CARE: any = ConstLogo.get(ConstLogoCode.ATP_CARE);
    private ATP_LAND: any = ConstLogo.get(ConstLogoCode.ATP_LAND);
    private ATP_MEDIA: any = ConstLogo.get(ConstLogoCode.ATP_MEDIA);
    private VOZ_VN: any = ConstLogo.get(ConstLogoCode.VOZ_VN);
    private WINERP: any = ConstLogo.get(ConstLogoCode.WINERP);

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        this.clickLogo(this.ATP_SOFTWARE.code);

        $("#inputDate").on("change", function() {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
            )
        }).trigger("change")

        if ($('#inputDate').attr('data-date') === "Invalid date") {
            $('#inputDate').attr('data-date', 'Ngày sinh');
        }
    }

    private clickLogo(logo) {
        $(".logo").removeClass("logo-active");
        $("#"+logo).addClass( "logo-active" );
        this.logo = logo;
    }

    private clickJoinGroup(url) {
        (<any>window).require('electron').shell.openExternal(url);
    }

    private nameKH: string;
    private phoneKH: string;
    private dateKH: string;
    clickSendContact() {
        $("#formContactKH").modal();
    }

    async onButtonSendContact() {
        if (!this.nameKH) {
            alert('Bạn chưa điền tên, nên chúng tôi không biết bạn là ai để hỗ trợ bạn được.');
            return false;
        }

        if (!this.phoneKH) {
            alert('Bạn chưa nhập số điện thoại, Mục này để chúng tôi có thể liên hệ với bạn để hỗ trợ.');
            return false;
        }

        if (!this.dateKH) {
            alert('Bạn chưa nhập ngày sinh.');
            return false;
        }
        let sendFormContactKh: SendFormContactKh = new SendFormContactKh(this.nameKH, this.phoneKH, this.dateKH, "Đăng ký Mua");
        await sendFormContactKh.task();
        alert('Gửi thông tin thành công, nhân viên chúng tôi sẽ liên hệ bạn sớm nhất có thể, cảm ơn bạn đã quan tâm ủng hộ.');
        $('#formContactKH').modal('hide')
    }

    clickFanPage() {
        (<any>window).require('electron').shell.openExternal('https://www.facebook.com/atpsoftware.tools/');
    }

    clickYoutube() {
        (<any>window).require('electron').shell.openExternal('https://www.youtube.com/c/atpsoftware?sub_confirmation=1');
    }
}
