import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-logo-atp-care',
    templateUrl: './logo-atp-care.component.html',
    styleUrls: ['./logo-atp-care.component.css','./../community-tab.component.css']
})
export class LogoAtpCareComponent implements OnInit {

    @Input() ATP_CARE: any;

    constructor() { }

    ngOnInit() {
    }

    private clickAtpCare(url) {
        (<any>window).require('electron').shell.openExternal(url);
    }

}
