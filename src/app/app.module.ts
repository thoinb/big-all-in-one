import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {enableProdMode, NgModule} from '@angular/core';
import {ScrollingModule} from "@angular/cdk/scrolling";
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {YouTubePlayerModule} from "@angular/youtube-player";

import {AppComponent} from './app.component';
import {LstSofewareTabComponent} from './lst-sofeware-tab/lst-sofeware-tab.component';
import {LicenseTabComponent} from './license-tab/license-tab.component';
import {AllInOneChildComponent} from './lst-sofeware-tab/all-in-one-child/all-in-one-child.component';
import {BigAllInOneChildComponent} from './lst-sofeware-tab/big-all-in-one-child/big-all-in-one-child.component';
import {AllInOneMobileVersionChildComponent} from "./lst-sofeware-tab/all-in-one-mobile-version-child/all-in-one-mobile-version-child.component";
import {UtilitiesForFreeChildComponent} from "./lst-sofeware-tab/utilities-for-free-child/utilities-for-free-child.component";
import {MenuHorizontalDiscoverComponent} from './discover-software-functionality-tab/menu-horizontal-discover/menu-horizontal-discover.component';
import {DiscoverSoftwareFunctionalityTabComponent} from './discover-software-functionality-tab/discover-software-functionality-tab.component';
import {DiscoverSimpleFacebookComponent} from './discover-software-functionality-tab/discover-simple-facebook/discover-simple-facebook.component';
import {DiscoverSimpleAccountComponent} from './discover-software-functionality-tab/discover-simple-account/discover-simple-account.component';
import {DiscoverAutoviralContentComponent} from "./discover-software-functionality-tab/discover-autoviral-content/discover-autoviral-content.component";
import {DiscoverSimpleInstagramComponent} from './discover-software-functionality-tab/discover-simple-instagram/discover-simple-instagram.component';
import {DiscoverSimpleZaloComponent} from './discover-software-functionality-tab/discover-simple-zalo/discover-simple-zalo.component';
import {DiscoverSimpleAdsComponent} from './discover-software-functionality-tab/discover-simple-ads/discover-simple-ads.component';
import {DiscoverCrmProfileComponent} from './discover-software-functionality-tab/discover-crm-profile/discover-crm-profile.component';
import {DiscoverSimpleTiktokComponent} from "./discover-software-functionality-tab/discover-simple-tiktok/discover-simple-tiktok.component";
import {DiscoverAtpCareComponent} from './discover-software-functionality-tab/discover-atp-care/discover-atp-care.component';
import {UseDownInstallComponent} from "./lst-sofeware-tab/all-in-one-child/use-down-install.component";
import {DiscoverSimpleLivestreamComponent} from './discover-software-functionality-tab/discover-simple-livestream/discover-simple-livestream.component';
import {DiscoverSimpleSeedingComponent} from './discover-software-functionality-tab/discover-simple-seeding/discover-simple-seeding.component';
import {DiscoverSimpleUidComponent} from './discover-software-functionality-tab/discover-simple-uid/discover-simple-uid.component';
import {CommunityTabComponent} from './community-tab/community-tab.component';
import {LogoAtpSoftwareComponent} from './community-tab/logo-atp-software/logo-atp-software.component';
import {LogoAtpAcademyComponent} from './community-tab/logo-atp-academy/logo-atp-academy.component';
import {LogoAtpWebComponent} from './community-tab/logo-atp-web/logo-atp-web.component';
import {LogoCvComVnComponent} from './community-tab/logo-cv-com-vn/logo-cv-com-vn.component';
import {LogoAtpCareComponent} from './community-tab/logo-atp-care/logo-atp-care.component';
import {LogoAtpLandComponent} from './community-tab/logo-atp-land/logo-atp-land.component';
import {LogoAtpMediaComponent} from './community-tab/logo-atp-media/logo-atp-media.component';
import {LogoWinErpComponent} from './community-tab/logo-win-erp/logo-win-erp.component';
import {DocumentsAndBooksTabComponent} from './documents-and-books-tab/documents-and-books-tab.component';
import {MenuHorizontalDocAndBookComponent} from './documents-and-books-tab/menu-horizontal-doc-and-book/menu-horizontal-doc-and-book.component';
import {BookSimpleFacebookComponent} from './documents-and-books-tab/book-simple-facebook/book-simple-facebook.component';
import {DocumentsSimpleFacebookComponent} from './documents-and-books-tab/documents-simple-facebook/documents-simple-facebook.component';
import {DocumentsSimpleAccountComponent} from './documents-and-books-tab/documents-simple-account/documents-simple-account.component';
import {BookSimpleAccountComponent} from './documents-and-books-tab/book-simple-account/book-simple-account.component';
import {DocumentsSimpleZaloComponent} from "./documents-and-books-tab/documents-simple-zalo/documents-simple-zalo.component";
import {DocumentsSimpleSeedingMobileComponent} from "./documents-and-books-tab/documents-simple-seeding-mobile/documents-simple-seeding-mobile.component";
import {DocumentsSimpleSeedingComponent} from "./documents-and-books-tab/documents-simple-seeding/documents-simple-seeding.component";
import {DocumentsSimpleInstagramComponent} from "./documents-and-books-tab/documents-simple-instagram/documents-simple-instagram.component";
import {DocumentsSimpleUidComponent} from "./documents-and-books-tab/documents-simple-uid/documents-simple-uid.component";
import { DocumentsAutoViralContentComponent } from './documents-and-books-tab/documents-auto-viral-content/documents-auto-viral-content.component';
import { DocumentsSimpleAdsComponent } from './documents-and-books-tab/documents-simple-ads/documents-simple-ads.component';
import { DocumentsCrmProfileComponent } from './documents-and-books-tab/documents-crm-profile/documents-crm-profile.component';
import { DocumentsSimpleTiktokComponent } from './documents-and-books-tab/documents-simple-tiktok/documents-simple-tiktok.component';
import { DocumentsAtpCareComponent } from './documents-and-books-tab/documents-atp-care/documents-atp-care.component';
import { DocumentsSimpleLivestreamComponent } from './documents-and-books-tab/documents-simple-livestream/documents-simple-livestream.component';
import {DocumentsSimpleFanpageComponent} from "./documents-and-books-tab/documents-simple-fanpage/documents-simple-fanpage.component";
import { CourseTabComponent } from './course-tab/course-tab.component';
import { CourseFreeComponent } from './course-tab/course-free/course-free.component';
import { CoursePaidComponent } from './course-tab/course-paid/course-paid.component';
import { PlayCourseVideosComponent } from './course-tab/course-free/play-course-videos/play-course-videos.component';
import { PlayVideoComponent } from './course-tab/course-free/play-course-videos/play-video/play-video.component';
import { CoursePaidVideosComponent } from './course-tab/course-paid/course-paid-videos/course-paid-videos.component';
import { PlayPaidVideoComponent } from './course-tab/course-paid/course-paid-videos/play-paid-video/play-paid-video.component';
import { LawTabComponent } from './law-tab/law-tab.component';


enableProdMode();

@NgModule({
    declarations: [
        AppComponent,
        LstSofewareTabComponent,
        LicenseTabComponent,
        AllInOneChildComponent,
        BigAllInOneChildComponent,
        AllInOneMobileVersionChildComponent,
        UtilitiesForFreeChildComponent,
        MenuHorizontalDiscoverComponent,
        DiscoverSoftwareFunctionalityTabComponent,
        DiscoverSimpleFacebookComponent,
        DiscoverSimpleAccountComponent,
        DiscoverAutoviralContentComponent,
        DiscoverSimpleInstagramComponent,
        DiscoverSimpleZaloComponent,
        DiscoverSimpleAdsComponent,
        DiscoverCrmProfileComponent,
        DiscoverSimpleTiktokComponent,
        DiscoverAtpCareComponent,
        UseDownInstallComponent,
        DiscoverSimpleLivestreamComponent,
        DiscoverSimpleSeedingComponent,
        DiscoverSimpleUidComponent,
        CommunityTabComponent,
        LogoAtpSoftwareComponent,
        LogoAtpAcademyComponent,
        LogoAtpWebComponent,
        LogoCvComVnComponent,
        LogoAtpCareComponent,
        LogoAtpLandComponent,
        LogoAtpMediaComponent,
        // LogoVozVnComponent, Bo voz vi ko hop tac
        LogoWinErpComponent,
        DocumentsAndBooksTabComponent,
        MenuHorizontalDocAndBookComponent,
        BookSimpleFacebookComponent,
        BookSimpleAccountComponent,
        DocumentsSimpleFacebookComponent,
        DocumentsSimpleAccountComponent,
        DocumentsSimpleZaloComponent,
        DocumentsSimpleSeedingMobileComponent,
        DocumentsSimpleSeedingComponent,
        DocumentsSimpleInstagramComponent,
        DocumentsSimpleUidComponent,
        DocumentsAutoViralContentComponent,
        DocumentsSimpleAdsComponent,
        DocumentsCrmProfileComponent,
        DocumentsSimpleTiktokComponent,
        DocumentsAtpCareComponent,
        DocumentsSimpleLivestreamComponent,
        DocumentsSimpleFanpageComponent,
        CourseTabComponent,
        CourseFreeComponent,
        CoursePaidComponent,
        PlayCourseVideosComponent,
        PlayVideoComponent,
        CoursePaidVideosComponent,
        PlayPaidVideoComponent,
        LawTabComponent,
    ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        ScrollingModule,
        ReactiveFormsModule,
        HttpClientModule,
        YouTubePlayerModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {

}
