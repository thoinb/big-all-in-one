import {AfterViewInit, Component, OnInit} from '@angular/core';
import {EventService} from "../../service/event-service";
import {FileProcessing} from "../../shared/request/file-processing";
import {Constant} from "../../shared/constrant/Constant";

declare let $: any;

@Component({
    selector: 'app-license-tab',
    templateUrl: './license-tab.component.html',
    styleUrls: ['./license-tab.component.css']
})
export class LicenseTabComponent implements OnInit, AfterViewInit {
    private file: FileProcessing = new FileProcessing();

    private nameATP: any;
    private phoneATP: string;
    private emailATP: any;
    private noteATP: any;
    private keyATP: any;
    private genderATP: any;
    private lstMapProductRender: Map<string, any> = new Map<string, any>();

    constructor(private eventService: EventService) {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.getInfoLicense();
        this.eventService.sendLstProductToLicenseTab.subscribe((lstMapProductRender) => {
            this.lstMapProductRender = lstMapProductRender;
        })
    }

    getInfoLicense() {
        this.file.getInfoLicense(async (result) => {
            if (result && result["HKCU\\Software\\ATP"] !== undefined && result["HKCU\\Software\\ATP"].values !== undefined) {
                try {
                    this.keyATP = null;
                    this.keyATP = result["HKCU\\Software\\ATP"].values["ATP-FACEBOOK"] !== undefined ? result["HKCU\\Software\\ATP"].values["ATP-FACEBOOK"].value !== undefined ? result["HKCU\\Software\\ATP"].values["ATP-FACEBOOK"].value.substring(13) : '' : '';
                    this.nameATP = result["HKCU\\Software\\ATP"].values["nameATP"] !== undefined ? result["HKCU\\Software\\ATP"].values["nameATP"].value !== undefined ? result["HKCU\\Software\\ATP"].values["nameATP"].value : '' : '';
                    this.phoneATP = result["HKCU\\Software\\ATP"].values["phoneATP"] !== undefined ? result["HKCU\\Software\\ATP"].values["phoneATP"].value !== undefined ? result["HKCU\\Software\\ATP"].values["phoneATP"].value : '' : '';
                    this.emailATP = result["HKCU\\Software\\ATP"].values["emailATP"] !== undefined ? result["HKCU\\Software\\ATP"].values["emailATP"].value !== undefined ? result["HKCU\\Software\\ATP"].values["emailATP"].value : '' : '';
                    this.noteATP = result["HKCU\\Software\\ATP"].values["noteATP"] !== undefined ? result["HKCU\\Software\\ATP"].values["noteATP"].value !== undefined ? result["HKCU\\Software\\ATP"].values["noteATP"].value : '' : '';
                    this.genderATP = result["HKCU\\Software\\ATP"].values["genderATP"] !== undefined ? result["HKCU\\Software\\ATP"].values["genderATP"].value !== undefined ? result["HKCU\\Software\\ATP"].values["genderATP"].value : '' : '';
                    if (this.genderATP === "1") {
                        $("#male").prop("checked", true);
                        $("#female").prop("checked", false);
                    } else {
                        $("#male").prop("checked", false);
                        $("#female").prop("checked", true);
                    }
                    if (this.nameATP && this.phoneATP && this.emailATP && this.genderATP) {
                        Constant.isUser = true;
                    } else {
                        Constant.isUser = false;
                    }
                } catch (e) {
                    Constant.isUser = false;
                }
            } else {
                Constant.isUser = false;
            }
        })
    }

    clickGender(gender) {
        if (gender === 'male') {
            $("#male").prop("checked", true);
            $("#female").prop("checked", false);
            this.genderATP = "1";
        } else {
            $("#male").prop("checked", false);
            $("#female").prop("checked", true);
            this.genderATP = "0";
        }
    }

    async saveInfoLicense() {

        if (!this.phoneNumber(this.phoneATP)) {
            return false
        }
        ;

        if (!this.validateEmail(this.emailATP)) {
            return false
        }
        ;

        if ($('#male').is(":checked")) {
            this.genderATP = 1;
            $("#male").prop("checked", true);
            $("#female").prop("checked", false);
        } else {
            $("#male").prop("checked", false);
            $("#female").prop("checked", true);
            this.genderATP = 0;
        }
        await this.file.saveRegeditATP(this.nameATP, this.phoneATP, this.noteATP, this.emailATP, this.keyATP, this.genderATP);
        await this.getInfoLicense();
        alert("Lưu thông tin thành công");


    }

    validateEmail(inputText) {
        let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (inputText.match(mailformat)) {
            return true;
        } else {
            alert("Email không hợp lệ");
            $('#emailATP').focus();
            return false;
        }
    }

    phoneNumber(test) {
        let phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (String(test).match(phoneno)) {
            return true;
        } else {
            alert("Số điện thoại không hợp lệ");
            return false;
        }
    }

    copyAllKeyToClipboard() {
        const {clipboard} = (<any>window).require('electron');
        let text = '';
        this.lstMapProductRender.forEach((product, key) => {
            if (product.keySoft) {
                text += product.keySoft + '\n';
            }
        });
        clipboard.writeText(text);
    }
}
