import {AfterViewInit, Component, OnInit} from "@angular/core";
import {EventService} from "../service/event-service";
import {FileProcessing} from "../shared/request/file-processing";
import {ConstantError} from "../shared/constrant/ConstantError";
import {LstMapProductRender} from "../shared/models/lstMapProductRender";
import {GetKeyAndStatusSoftServer} from "../request/GET/get-key-and-status-soft-server";
import {ProductStatus} from "../shared/enum/ProductStatus";
import {GetIdMachineForFirebaseMonitor} from "../firebase/get-id-machine-for-firebase-monitor";
import {FirebaseMonitor} from "../firebase/firebase-monitor";
import {Constant} from "../shared/constrant/Constant";
import {InitFcm} from "../firebase/fcm/init-fcm";
import {Utils} from "../shared/utils/utils";
import {FirebaseGetNewsUpdate} from "../firebase/firebase-get-news-update";
import {NewsUpdate} from "../shared/models/news-update";

declare var $: any;
@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})


export class AppComponent implements OnInit, AfterViewInit {
    private file: FileProcessing = new FileProcessing();
    private productStatus: typeof ProductStatus = ProductStatus;
    private listNewsUpdate: Array<NewsUpdate> = new Array<NewsUpdate>();

    constructor(private eventService: EventService) {
    }

    private lstMapProductRender = LstMapProductRender.lstMapProductRender;

    ngOnInit() {

    }

    async ngAfterViewInit() {
        FirebaseGetNewsUpdate.getUpdate((listNews) => {
            $('#btnSlideDownUpdateNews').css("display", "block");
            this.listNewsUpdate = this.listNewsUpdate.concat(listNews).reverse();
            if (this.listNewsUpdate && this.listNewsUpdate.length > 0) {
                if (!this.listNewsUpdate[0].seen) {
                    $('#btnSlideDownUpdateNews').addClass("animate__animated animate__headShake animate__infinite");
                }
            }
        });
        // let path = (<any>window).require('path');
        //         let pathAsar = path.join(__dirname, '../../', 'app');
        //         let keyHex = await Utils.execute('"' + pathAsar + '\\info.exe" simple-facebook');
        //         if (keyHex !== undefined) {
        //             keyHex = keyHex.replace(/(\r\n|\n|\r)/gm, "");
        //             keyHex = keyHex.trim();
        //         }

        //         alert('ATP-BIG-' + keyHex);

        try {
            let path = (<any>window).require('path');
            let pathAsar = path.join(__dirname, '../../', 'app');
            let mainWindow = (<any>window).require('electron').remote.getCurrentWindow();
            if (mainWindow['version'] === undefined || mainWindow['version'] !== Constant.versionApp) {
                let pathMain = path.join(pathAsar, '/electron/dist', 'main.js');
                await this.downloadFile('http://crm.alosoft.vn/bigallinone_05_05_2020/main.js', pathMain);
                alert(`Chương trình đã fix lỗi:            
                    => Chương trình sẽ tự động tắt để cập nhật, Vui lòng bật lại`);
                (<any>window).require('electron').remote.app.quit();
                return;
            }
            ;

            document.getElementById("myNav").style.width = "100%";

            this.checkStatsSofeware(this.lstMapProductRender);
            await this.scanKeyAndStatusSoft(this.lstMapProductRender).then(() => {
                this.sendLstProductToLicenseTab(this.lstMapProductRender)
            });

            document.getElementById("myNav").style.width = "0%";
            await InitFcm.init().catch(reason => {
                console.log(reason);
            });
            let getMachineID = new GetIdMachineForFirebaseMonitor();
            getMachineID.task().then(value => {
                FirebaseMonitor.online(getMachineID.idMachine, Constant.TOKEN_FCM);
            });
        } catch (e) {
            alert(e);
            (<any>window).require('electron').remote.app.quit();
            return;
        }
    }

    async downloadFile(url, savePath) {
        return new Promise<any>((resolve, reject) => {
            const http = (<any>window).require('http');
            const fs = (<any>window).require('fs');
            const file = fs.createWriteStream(savePath);
            const request = http.get(url, function (response) {
                response.pipe(file);
                file.on('finish', function () {
                    resolve();
                });
                file.on('error', function (err) {
                    reject(err);
                });
            }).on('error', function (err) { // Handle errors
                reject(err);
            });
        });
    }

    private sendLstProductToLicenseTab(lstMapProductRender) {
        this.eventService.sendLstProductToLicenseTab.emit(lstMapProductRender);
    }

    private checkStatsSofeware(lstMapProductRender) {
        this.eventService.appLstMapProductRenderToButtonAllInOne.emit(this.lstMapProductRender);
        this.file.checkStatusSofeware(lstMapProductRender, (productRender) => {
            this.eventService.appProductRenderToButtonAllInOne.emit(productRender);
        });
    }

    private async scanKeyAndStatusSoft(lstMapProductRender) {
        let lstKeyRegistry = await this.getKeyRegistry();
        if (lstKeyRegistry === false) {
            return false;
        }
        // console.log("lstKeyRegistry" + lstKeyRegistry);
        for (let key of Array.from(lstMapProductRender.keys())) {
            if (!lstMapProductRender.get(key).keyRegistry || lstMapProductRender.get(key).keyRegistry.length === 0) {
                continue;
            }
            let keyRegistry = null;
            try {
                keyRegistry = lstKeyRegistry[lstMapProductRender.get(key).keyRegistry].value;
            } catch (e) {
                continue;
            }
            if (keyRegistry) {
                let getKeyAndStatusSoftServer = new GetKeyAndStatusSoftServer(keyRegistry);
                await getKeyAndStatusSoftServer.task().catch((e) => {
                    alert(ConstantError.LOGGER_ERROR_ATP + Utils.getError(e));
                });
                let indexOf = getKeyAndStatusSoftServer.getData.indexOf(" - ");//"ATP-FACEBOOK-4317392 - (còn 0/7 ngày)"
                if (lstMapProductRender.get(key).status === this.productStatus.HAS_BEEN_FILE_INSTALLED) {
                    this.getKeySoft(getKeyAndStatusSoftServer.getData, indexOf, lstMapProductRender.get(key));
                    this.getStatusDateSoft(getKeyAndStatusSoftServer.getData, indexOf, lstMapProductRender.get(key));
                }
            }
        }
    }

    private async getKeyRegistry() {
        return this.file.getKeyRegistry();
    }

    private getKeySoft(keyRegistry, indexOf, productRender) {
        productRender.keySoft = keyRegistry.substring(0, indexOf);
    }

    private getStatusDateSoft(keyRegistry, indexOf, productRender) {
        let statusDate = keyRegistry.substring(indexOf + 3);
        if (statusDate) {
            let numberDate = statusDate.replace("(còn ", "");
            numberDate = numberDate.replace(" ngày)", "");
            let lstNumberDate = numberDate.split("/");

            if (Number(lstNumberDate[0]) === 0) {
                productRender.status = this.productStatus.COPYRIGHT_HAS_EXPIRED;
                productRender.statusDateView = "Hết Hạn";
                productRender.statusDateValue = numberDate;
                productRender.statusDate = 0; // hết hạn
            } else
                productRender.status = this.productStatus.COPYRIGHT_IS_STILL_VALID;
            productRender.statusDateValue = numberDate;
            productRender.statusDateView = "Còn " + productRender.statusDateValue + " ngày";
            if (statusDate === "(Gói dùng thử)") {
                productRender.statusDateValue = "0/7";
                productRender.statusDateView = "Gói dùng thử";
            }
            productRender.statusDate = 1; // còn hạn
        }
    }

    private tab1: boolean = true;
    private tab2: boolean = false;
    private tab3: boolean = false;
    private tab4: boolean = false;
    private tab5: boolean = false;
    private tab6: boolean = false;
    private tab7: boolean = false;

    clickTab(text) {
        switch (text) {
            case 'tab1':
                this.tab1 = true;
                this.tab2 = false;
                this.tab3 = false;
                this.tab4 = false;
                this.tab5 = false;
                this.tab6 = false;
                this.tab7 = false;
                break;
            case 'tab2':
                this.tab1 = false;
                this.tab2 = true;
                this.tab3 = false;
                this.tab4 = false;
                this.tab5 = false;
                this.tab6 = false;
                this.tab7 = false;
                break;
            case 'tab3':
                this.tab1 = false;
                this.tab2 = false;
                this.tab3 = true;
                this.tab4 = false;
                this.tab5 = false;
                this.tab6 = false;
                this.tab7 = false;
                break;
            case 'tab4':
                this.tab1 = false;
                this.tab2 = false;
                this.tab3 = false;
                this.tab4 = true;
                this.tab5 = false;
                this.tab6 = false;
                this.tab7 = false;
                break;
            case 'tab5':
                this.tab1 = false;
                this.tab2 = false;
                this.tab3 = false;
                this.tab4 = false;
                this.tab5 = true;
                this.tab6 = false;
                this.tab7 = false;
                break;
            case 'tab6':
                this.tab1 = false;
                this.tab2 = false;
                this.tab3 = false;
                this.tab4 = false;
                this.tab5 = false;
                this.tab6 = true;
                this.tab7 = false;
                break;
            case 'tab7':
                this.tab1 = false;
                this.tab2 = false;
                this.tab3 = false;
                this.tab4 = false;
                this.tab5 = false;
                this.tab6 = false;
                this.tab7 = false;
                break;
            default:
        }
    }

    openModalUpdates() {
        $('#modalNewsUpdate').modal('show');
        if (this.listNewsUpdate && this.listNewsUpdate.length > 0 && !this.listNewsUpdate[0].seen) {
            $('#btnSlideDownUpdateNews').removeClass("animate__animated animate__headShake animate__infinite");
            FirebaseGetNewsUpdate.saveLastTimestampSeenUpdate(this.listNewsUpdate[0].timestamp);
        }
    }
}
