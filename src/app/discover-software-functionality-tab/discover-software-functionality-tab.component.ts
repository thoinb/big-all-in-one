import {Component, OnInit} from '@angular/core';
import {ConstProductKey} from "../../shared/const/ConstProduct";

@Component({
    selector: 'app-discover-software-functionality-tab',
    templateUrl: './discover-software-functionality-tab.component.html',
    styleUrls: ['./discover-software-functionality-tab.component.css']
})
export class DiscoverSoftwareFunctionalityTabComponent implements OnInit {

    private chooseTypeMenuDiscover: string = "";

    constructor() {
    }

    ngOnInit() {
    }

    // li - 1
    private clickSimpleFacebook() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_FACEBOOK;
    }
    // li - 2
    private clickSimpleAccount() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_ACCOUNT;
    }

    // li - 3
    private clickSimpleInstagram() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_INSTAGRAM;
    }

    // li - 4
    private clickSimpleZalo() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_INSTAGRAM;
    }

    // li - 5
    private clickAutoviralContent() {
        return this.chooseTypeMenuDiscover = ConstProductKey.AUTOVIRAL_CONTENT;
    }

    private clickSimpleAds() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_ADS;
    }

    // li - 6
    private clickCRMProfile() {
        return this.chooseTypeMenuDiscover = ConstProductKey.CRM_PROFILE;
    }

    // li - 7
    private clickSimpleTiktok() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_TIKTOK;
    }

    // li - 8
    private clickAtpCare() {
        return this.chooseTypeMenuDiscover = ConstProductKey.ATP_CARE;
    }

    // li - 9
    private clickSimpleLiveStream() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_LIVESTREAM;
    }

    // li - 10
    private clickSimpleUID() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_UID;
    }

    // li - 11
    private clickSimpleSeeding() {
        return this.chooseTypeMenuDiscover = ConstProductKey.SIMPLE_SEEDING;
    }

}
