import {Component, Input, OnInit, AfterViewInit} from '@angular/core';
import SendFormContactKh from "../../../request/GET/send-form-contact-kh";
import * as moment from 'moment'

declare let $: any;

@Component({
    selector: 'app-menu-horizontal-discover',
    templateUrl: './menu-horizontal-discover.component.html',
    styleUrls: ['./menu-horizontal-discover.component.css']
})
export class MenuHorizontalDiscoverComponent implements OnInit, AfterViewInit {

    @Input() disabled: true;
    @Input() chooseTypeMenuDiscover: string = "";

    constructor() {

    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        $("#inputDate").on("change", function() {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
            )
        }).trigger("change")

        if ($('#inputDate').attr('data-date') === "Invalid date") {
            $('#inputDate').attr('data-date', 'Ngày sinh');
        }
    }

    private nameKH: string;
    private phoneKH: string;
    private dateOfBirthKH: string = '';

    private async discoverOrder() {
        
        $("#formContactKH").modal();
    }

    async onButtonSendContact(chooseTypeMenuDiscover) {
        
        if (!this.nameKH) {
            alert('Bạn chưa điền tên, nên chúng tôi không biết bạn là ai để hỗ trợ bạn được.');
            return false;
        }

        if (!this.phoneKH) {
            alert('Bạn chưa nhập số điện thoại, Mục này để chúng tôi có thể liên hệ với bạn để hỗ trợ.');
            return false;
        }

        if (!this.dateOfBirthKH) {
            alert('Bạn chưa nhập ngày sinh.');
            return false;
        }
        
        let sendFormContactKh: SendFormContactKh = new SendFormContactKh(this.nameKH, this.phoneKH,this. dateOfBirthKH, chooseTypeMenuDiscover);
        await sendFormContactKh.task();
        alert('Gửi thông tin thành công, nhân viên chúng tôi sẽ liên hệ bạn sớm nhất có thể, cảm ơn bạn đã quan tâm ủng hộ.');
        $('#formContactKH').modal('hide')
    }

}
