import { Component, OnInit, Input } from '@angular/core';
import { Course } from 'src/shared/models/course';

@Component({
	selector: 'app-course-free',
	templateUrl: './course-free.component.html',
	styleUrls: ['./course-free.component.css']
})

export class CourseFreeComponent implements OnInit {
	
	@Input() freeCourse: Course[];

	public selectedCourse: Course;
	public tabPlay: boolean = false;
	public title: string;

	constructor() { };

	ngOnInit() { ;};

	openPlayTab(title: string, selectedCourse: Course) {
		this.tabPlay = true;
		this.title = title;
		this.selectedCourse = selectedCourse;
	};

	backCourse() {
		this.tabPlay = false;
	};

};
