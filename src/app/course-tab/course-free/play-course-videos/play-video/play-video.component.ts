import { Component, Input, OnChanges } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Video } from 'src/shared/models/Video';

@Component({
	selector: 'app-play-video',
	templateUrl: './play-video.component.html',
	styleUrls: ['./play-video.component.css']
})
export class PlayVideoComponent implements OnChanges {

	@Input() linkSend: Video = new Video;

	public urlSafe: SafeResourceUrl;

	private url: string;

	constructor(public sanitizer: DomSanitizer) {

	}

	ngOnChanges() {
		this.url = "https://www.youtube.com/embed/" + this.linkSend.idVideo;
		this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
	};
}
