import { Constant } from './../../../../shared/constrant/Constant';
import { Video } from 'src/shared/models/Video';
import { Course } from './../../../../shared/models/course';
import { Component, OnInit, Input } from '@angular/core';
import { GetVideos } from 'src/request/GET/get-videos-server';
import { Utils } from 'src/shared/utils/utils';

@Component({
	selector: 'app-play-course-videos',
	templateUrl: './play-course-videos.component.html',
	styleUrls: ['./play-course-videos.component.css']
})
export class PlayCourseVideosComponent implements OnInit {

	@Input() selectedCourse: Course;

	public linkSend: Video = new Video;
	public checkOpenVideo: boolean = false;
	public isLoading: boolean = true;

	public listVideos: Video[] = [];
	public data: any;

	constructor() { }

	ngOnInit() {
		this.getVideos();
	};

	private async getVideos() {
		this.isLoading = true;
		try {
			let getVideos = new GetVideos(this.selectedCourse.idCourse);
			await getVideos.task();
			// let data = getVideos.getData;
			// if (data.status === Constant.statusRequest.ERROR) {
			// 	alert(data.message);
			// 	this.isLoading = false;
			// 	return false;
			// }
			// data.result.forEach(element => {
			// 	let obj = new Video;
			// 	let str: string[];
			// 	let str1: string[];
			// 	let str2: string[];
			// 	str = element[1].split("&")
			// 	str1 = str[0].split("/")
			// 	let str1Len = str1.length;
			// 	str2 = str1[str1Len - 1].split("=");
			// 	let str2Len = str2.length;
			// 	obj.title = Utils.decodeHTMLEntities(element[0]);
			// 	obj.checkTitle = 1;
			// 	obj.link = element[1];
			// 	obj.idVideo = str2[str2Len - 1];
			// 	obj.linkImg = "https://img.youtube.com/vi/" + obj.idVideo + "/default.jpg";
			// 	this.listVideos.push(obj);
			// });
			this.listVideos = getVideos.arrVideos;
			this.isLoading = false;
		} catch (error) {
			Utils.showError('CoursePaidVideosComponent.getVideos()', error);
			this.isLoading = false;
			return false;
		}
	};

	public setLink(link: Video) {
		this.linkSend = link;
		this.checkOpenVideo = true;
	};

}
