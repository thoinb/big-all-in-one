import { Component, OnInit } from '@angular/core';
import { GetDataCourse } from 'src/request/GET/get-course-videos-server';
import { Utils } from "../../shared/utils/utils";
import { Course } from "../../shared/models/course";
import { Constant } from "../../shared/constrant/Constant";

@Component({
    selector: 'app-course-tab',
    templateUrl: './course-tab.component.html',
    styleUrls: ['./course-tab.component.css']
})

export class CourseTabComponent implements OnInit {

    public freeCourse: Array<Course> = new Array<Course>();
    public paidCourse: Array<Course> = new Array<Course>();
    public isLoading: boolean = true;

    private typeCourse: { [key: string]: number } = Constant.typeCourse;

    constructor() {
    };

    async ngOnInit() {
        await this.getDataCourse();
    };

    private async getDataCourse() {
        try {
            this.isLoading = true;
            let getDataCourse: GetDataCourse = new GetDataCourse();
            await getDataCourse.task();
            let lstCourse = getDataCourse.lstCourse ? getDataCourse.lstCourse : null;
            if (lstCourse) {
                lstCourse.forEach((course: Course) => {
                    if (course.typeCourse == this.typeCourse.FREE_COURSE) {
                        this.freeCourse.push(course);
                    } else if (course.typeCourse == this.typeCourse.PAID_COURSE) {
                        this.paidCourse.push(course);
                    }
                });
            }
            this.isLoading = false;
        } catch (e) {
            alert('CourseTabComponent.getDataCourse() ' +  Utils.getError(e));
            this.isLoading = false;
        }
    };

};
