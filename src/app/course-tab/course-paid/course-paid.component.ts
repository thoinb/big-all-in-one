import { Course } from './../../../shared/models/course';
import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-course-paid',
	templateUrl: './course-paid.component.html',
	styleUrls: ['./course-paid.component.css']
})
export class CoursePaidComponent implements OnInit {
	
	@Input() paidCourse: Course[];

	public selectedCourse: Course;
	public tabPlay: boolean = false;
	public title: string;

	constructor() { };

	ngOnInit() { };

	public openPlayTab(title: string, selectedCourse: Course) {
		this.tabPlay = true;
		this.title = title;
		this.selectedCourse = selectedCourse;
	};

	public backCourse() {
		this.tabPlay = false;
	};

}
