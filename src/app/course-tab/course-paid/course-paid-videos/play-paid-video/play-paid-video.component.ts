import { Video } from 'src/shared/models/Video';
import { Component, Input, OnChanges } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
	selector: 'app-play-paid-video',
	templateUrl: './play-paid-video.component.html',
	styleUrls: ['./play-paid-video.component.css']
})
export class PlayPaidVideoComponent implements OnChanges {

	@Input() linkSend: Video;

	public urlSafe: SafeResourceUrl;

	private url: string;

	public isLegit: boolean = false;


	constructor(public sanitizer: DomSanitizer) {

	};

	ngOnChanges() {
		this.url = "https://www.youtube.com/embed/" + this.linkSend.idVideo;
		/* this.url = "https://atpacademy.vn/wp-content/uploads/2020/03/huong-dan-ke-chan-may.mp4"; */
		this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
		if (this.linkSend.idVideo) {
			this.isLegit = true;
		} else {
			this.isLegit = false;
		}
	}

	public clickAtpAcademy(url) {
		(<any>window).require('electron').shell.openExternal(url);
	}

}
