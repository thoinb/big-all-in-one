import { Constant } from './../../../../shared/constrant/Constant';
import { Course } from './../../../../shared/models/course';
import { Component, OnInit, Input } from '@angular/core';
import { GetVideos } from 'src/request/GET/get-videos-server';
import { Utils } from 'src/shared/utils/utils';
import { Video } from 'src/shared/models/Video';

@Component({
	selector: 'app-course-paid-videos',
	templateUrl: './course-paid-videos.component.html',
	styleUrls: ['./course-paid-videos.component.css']
})
export class CoursePaidVideosComponent implements OnInit {

	@Input() selectedCourse : any;

	public linkSend = new Video;
	public checkOpenVideo: boolean = false;

	public listVideos: Video[] = [];

	public isLoading: boolean = true;

	constructor() { };

	ngOnInit() {
		this.getVideos();
	};

	private async getVideos() {
		this.isLoading = true;
		try {
			let getVideos = new GetVideos(this.selectedCourse.idCourse);
			await getVideos.task();
			// let data = getVideos.getData;
			// if (data.status === Constant.statusRequest.ERROR) {
			// 	alert(data.message);
			// 	this.isLoading = false;
			// 	return false;
			// }
			// mp4
/* 			data.result.forEach(element => {
				let obj = new Video;
				obj.title = Utils.decodeHTMLEntities(element[0]);
				obj.link = element[1] ? element[1] : null;
				obj.linkImg = this.selectedCourse.imageCourse ? this.selectedCourse.imageCourse : null;
				this.listVideos.push(obj);
			}); */

			// data.result.forEach(element => {
			// 	let obj = new Video;
			// 	if (element[1]) {
			// 		let str: string[];
			// 		let str1: string[];
			// 		let str2: string[];
			// 		str = element[1].split("&");
			// 		str1 = str[0].split("/")
			// 		let str1Len = str1.length;
			// 		str2 = str1[str1Len - 1].split("=");
			// 		let str2Len = str2.length;
			// 		obj.title = Utils.decodeHTMLEntities(element[0]);
			// 		obj.checkTitle = 1;
			// 		obj.link = element[1];
			// 		obj.idVideo = str2[str2Len - 1];
			// 		obj.linkImg = "https://img.youtube.com/vi/" + obj.idVideo + "/default.jpg";
			// 		this.listVideos.push(obj);
			// 	} else {
			// 		obj.title = Utils.decodeHTMLEntities(element[0]);
			// 		obj.checkTitle = 1;
			// 		obj.link = element[1];
			// 		obj.idVideo = null;
			// 		obj.linkImg = this.selectedCourse.imageCourse;
			// 		this.listVideos.push(obj);
			// 	}
			// });
			this.listVideos = getVideos.arrVideos;
			this.isLoading = false;
		} catch (error) {
			Utils.showError('CoursePaidVideosComponent.getVideos()', error);
			this.isLoading = false;
			return false;
		}
	};

	public setLink(link: Video) {
		this.linkSend = link;
		this.checkOpenVideo = true;
	}

};
