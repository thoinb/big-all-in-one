import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-zalo',
    templateUrl: './documents-simple-zalo.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleZaloComponent implements OnInit, AfterViewInit {

    private players: any = new Array();

    private playerInfoListSimpleZalo: any = [{
        id: 'playerSimpleZalo1', // Hướng dẫn sử dụng phần mềm Simple Zalo
        height: 'auto',
        width: 'auto',
        videoId: '12UIiZDVmuI'
    }, ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-zalo').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleZalo === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleZalo.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleZalo[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/1eSdAArEfqLNhh4fflBX1QA6IgOo1sGcf/view";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
