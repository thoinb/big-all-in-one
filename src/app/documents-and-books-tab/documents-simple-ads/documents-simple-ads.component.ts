import { Component, OnInit } from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-ads',
    templateUrl: './documents-simple-ads.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleAdsComponent implements OnInit {

    private players: any = new Array();

    private playerInfoListSimpleAds: any = [{
        id: 'playerSimpleAds1', // Hướng dẫn sử dụng phần mềm Simple UID
        height: 'auto',
        width: 'auto',
        videoId: 'sqlpnN25EL0'
    } ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-ads').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady()

    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleAds === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleAds.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleAds[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://sum.vn/simpleads";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
