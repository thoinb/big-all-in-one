import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-uid',
    templateUrl: './documents-simple-uid.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleUidComponent implements OnInit, AfterViewInit {

    private players: any = new Array();

    private playerInfoListSimpleUid: any = [{
        id: 'playerSimpleUid1', // Hướng dẫn sử dụng phần mềm Simple UID
        height: 'auto',
        width: 'auto',
        videoId: 'frX-Hj0vnNg'
    }, ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-uid').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleUid === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleUid.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleUid[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://sum.vn/spuidv2";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
