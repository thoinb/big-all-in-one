import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-seeding-mobile',
    templateUrl: './documents-simple-seeding-mobile.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleSeedingMobileComponent implements OnInit, AfterViewInit {

    private players: any = [];

    private playerInfoListSimpleSeedingMobile: any = [{
        id: 'playerSimpleSeedingMobile1', // Hướng dẫn sử dụng phần mềm Simple SeedingMobile
        height: 'auto',
        width: 'auto',
        videoId: 'lJ2d-uJ9L-0'
    }, ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-seeding-mobile').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleSeedingMobile === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleSeedingMobile.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleSeedingMobile[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/14KxUmzZeMJXtyHVuYqPcKW02KU3ewxrz/view";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
