import {AfterViewInit, Component, OnInit} from '@angular/core';

declare let $: any;

@Component({
    selector: 'app-documents-crm-profile',
    templateUrl: './documents-crm-profile.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsCrmProfileComponent implements OnInit, AfterViewInit {
    private players: any = new Array();

    private playerInfoListCrmProFile: any = [
        {
            id: 'playerSimpleCrmProFile1', //Hướng dẫn tải và cài đặt phần mềm CRM Profile
            height: 'auto',
            width: 'auto',
            videoId: 'KfF1994rxIs'
        }, {
            id: 'playerSimpleCrmProFile2', //Hướng dẫn tạo trạng thái khách hàn
            height: 'auto',
            width: 'auto',
            videoId: 'ibO_TlPJ5HA'
        }, {
            id: 'playerSimpleCrmProFile3', //Hướng dẫn đặt lịch hẹn với khách hàn
            height: 'auto',
            width: 'auto',
            videoId: 'Qd1xff2d1IY'
        }, {
            id: 'playerSimpleCrmProFile4', //Hướng dẫn gửi tin nhắn Facebook theo từng trạng thái khách hàng:
            height: 'auto',
            width: 'auto',
            videoId: 'ZR8ByV01coY'
        }, {
            id: 'playerSimpleCrmProFile5', //Kinh nghiệm ứng dụng công cụ CRM Profile hiệu quả
            height: 'auto',
            width: 'auto',
            videoId: 'qu7EfY1dFTo'
        }, {
            id: 'playerSimpleCrmProFile6', //Hướng dẫn sử dụng chatbot, quản lý đơn hàng, quản lý khách hàng trên CRM Profilv
            height: 'auto',
            width: 'auto',
            videoId: 'hag68RdRXnc'
        }
    ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-crm-pro-file').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListCrmProFile === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListCrmProFile.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListCrmProFile[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/1YXTgd6Qas7msVvdaXfXCY4anVBrfkDGC/view";
        (<any>window).require('electron').shell.openExternal(src);
    }

}
