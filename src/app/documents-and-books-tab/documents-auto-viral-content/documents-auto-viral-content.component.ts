import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-auto-viral-content',
    templateUrl: './documents-auto-viral-content.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsAutoViralContentComponent implements OnInit ,AfterViewInit{

    private players: any = new Array();

    private playerInfoListSimpleAutoViralContent: any = [
        {
            id: 'playerSimpleAutoViralContent1', //Video hướng dẫn đăng nhậ
            height: 'auto',
            width: 'auto',
            videoId: '_qyfOEksFXU'
        }, {
            id: 'playerSimpleAutoViralContent2', // Hướng dẫn sử dụng phần mềm Simple Zalo
            height: 'auto',
            width: 'auto',
            videoId: 'fsDhdXVeVLs'
        }
    ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-auto-viral-content').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleAutoViralContent === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleAutoViralContent.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleAutoViralContent[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://sum.vn/autoviral";
        (<any>window).require('electron').shell.openExternal(src);
    }

}
