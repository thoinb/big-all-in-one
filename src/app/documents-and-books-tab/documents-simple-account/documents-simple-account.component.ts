import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-account',
    templateUrl: './documents-simple-account.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleAccountComponent implements OnInit, AfterViewInit {

    private players: any = new Array();

    private playerInfoListSimpleAccount: any = [{
        id: 'playerSimpleAccount1', 
        height: 'auto',
        width: 'auto',
        videoId: 'GWyOlzCrwRE'
    }, {
        id: 'playerSimpleAccount2', 
        height: 'auto',
        width: 'auto',
        videoId: 'FL2HS1YHJ0U'
    }, {
        id: 'playerSimpleAccount3', // Hướng dẫn đặt lịch tự động comment trên Simple Account
        height: 'auto',
        width: 'auto',
        videoId: 'BiOHElwKFd4'
    }, {
        id: 'playerSimpleAccount4', // Hướng dẫn đặt lịch tự động đăng bài trên Facebook cá nhân bằng Simple Acount
        height: 'auto',
        width: 'auto',
        videoId: 'PVHYQKfV78E'
    }, {
        id: 'playerSimpleAccount5', // Hướng dẫn đặt lịch tự động tham gia, đăng bài, thêm bạn vào nhóm trên Facebook
        height: 'auto',
        width: 'auto',
        videoId: 'Sy-62-DqcIQ'
    }, {
        id: 'playerSimpleAccount6', // Hướng dẫn tự động trả lời comment trên Facebook:
        height: 'auto',
        width: 'auto',
        videoId: '0y0lIXoRHsc'
    }, {
        id: 'playerSimpleAccount7', // Back up hình ảnh bạn bè trên Facebook để tránh checkpoin
        height: 'auto',
        width: 'auto',
        videoId: 'yhglqtujP_8'
    }, {
        id: 'playerSimpleAccount8', // Hướng dẫn chọc ghẹo và CMSN bạn bè Facebook tự độn
        height: 'auto',
        width: 'auto',
        videoId: 'dXVgh9UKkiw'
    }/* , {
        id: 'playerSimpleAccount9', // Hướng dẫn sao chép bài viết cố định & Profile
        height: 'auto',
        width: 'auto',
        videoId: 'nJpoeolaeE4'
    }, {
        id: 'playerSimpleAccount10', // Hướng dẫn sử dụng tab "QUẢN LÝ TƯƠNG TÁC" trong Simple Faceboo
        height: 'auto',
        width: 'auto',
        videoId: 'PFnVoTplyCM'
    }, {
        id: 'playerSimpleAccount11', // Mời bạn bè like Fanpage
        height: 'auto',
        width: 'auto',
        videoId: 'wm9cKOT-Chg'
    }, {
        id: 'playerSimpleAccount12', // Reply tin nhắn tự động trên trang cá nhân
        height: 'auto',
        width: 'auto',
        videoId: 'xEP2y4XI7r0'
    }, {
        id: 'playerSimpleAccount13', // GRời khỏi nhóm tự động trên trang cá nhân
        height: 'auto',
        width: 'auto',
        videoId: 'oJhMvdmxSyg'
    }, {
        id: 'playerSimpleAccount14', // Uptop bài viết trên Group Facebook
        height: 'auto',
        width: 'auto',
        videoId: '0tWvC1-bon8'
    }, {
        id: 'playerSimpleAccount15', // Cách ramdom các nội dung comment & inbox trên Simple Account
        height: 'auto',
        width: 'auto',
        videoId: 'Lf-U5j7W2Z8'
    }, {
        id: 'playerSimpleAccount16', // Hướng Dẫn Đăng Nhập Simple account Bằng Giả Lập Chrome
        height: 'auto',
        width: 'auto',
        videoId: 'urf_7sguUY0'
    } */];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-account').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleAccount === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleAccount.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleAccount[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/1GhI5xgQAsKrWsfB_iLE87FnOZfHeNJk8/view";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
