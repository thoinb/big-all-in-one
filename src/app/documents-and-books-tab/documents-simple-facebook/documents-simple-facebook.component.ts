import {AfterViewInit, Component, OnInit} from '@angular/core';

declare let $: any;

@Component({
    selector: 'app-documents-simple-facebook',
    templateUrl: './documents-simple-facebook.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleFacebookComponent implements OnInit, AfterViewInit {

    private players: any = new Array();
    private playerInfoListSimpleFacebook: any = [{
        id: 'player1', // Tải và cài đặt phần mềm Simple Facebook mới nhất 2019
        height: 'auto',
        width: 'auto',
        videoId: 'Itif31lLkEs'
    }, {
        id: 'player2', // Hướng dẫn đăng nhập tài khoản Facebook vào Simple Facebook v2
        height: 'auto',
        width: 'auto',
        videoId: '4zqsGyWqshY'
    }, {
        id: 'player3', // Tiến trình xử lý là gì? Chức năng mới được update trong Simple Facebook
        height: 'auto',
        width: 'auto',
        videoId: 'mYiQdtGNgCs'
    }, {
        id: 'player4', // Hướng dẫn kết bạn với danh sách bạn bè của một người bất kỳ
        height: 'auto',
        width: 'auto',
        videoId: '5WCCLXgpkfI'
    }, {
        id: 'player5', // Kết bạn với danh sách đề xuất Facebook
        height: 'auto',
        width: 'auto',
        videoId: '7082m-j88cQ'
    }, {
        id: 'player6', // Kết bạn với danh sách follower của 1 người bất kỳ
        height: 'auto',
        width: 'auto',
        videoId: '4Z_7ah1q-0k'
    }, {
        id: 'player7', // Kết bạn tự động với danh sách Like - Comment - Share bài viết bất kỳ:
        height: 'auto',
        width: 'auto',
        videoId: '84WYL5rz7vU'
    }, {
        id: 'player8', // Kết bạn với danh sách data khách hàng SĐT - EMAIL - UID
        height: 'auto',
        width: 'auto',
        videoId: 'GNP2cgrBjq0'
    }, {
        id: 'player9', // Chúc mừng sinh nhật bạn bè Facebook
        height: 'auto',
        width: 'auto',
        videoId: 'b_MNPTYl-ps'
    }, {
        id: 'player10', // Hủy lời mời kết bạn khi full 1000 lượt
        height: 'auto',
        width: 'auto',
        videoId: 'FLdWjS9TZv8'
    }, {
        id: 'player11', // Gửi tin nhắn Facebook tự động trên trang cá nhân
        height: 'auto',
        width: 'auto',
        videoId: 'tT6q7ggDjpo'
    }, {
        id: 'player12', // Hướng dẫn kết bạn theo ĐỘ TUỔI - GIỚI TÍNH - KHU VỰ
        height: 'auto',
        width: 'auto',
        videoId: '9kL8A41Ui8k'
    }, {
        id: 'player13', // Gửi tin nhắn trên Fanpage bằng Simple Facebook
        height: 'auto',
        width: 'auto',
        videoId: 'a0CsuXbjBJM'
    }];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-facebook').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady()
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleFacebook === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleFacebook.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleFacebook[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/1n2E2_LRpCLZPH88IlJnn4ko015pfbJE3/view";
        (<any>window).require('electron').shell.openExternal(src);
    }

}
