import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ConstProductKey} from "../../shared/const/ConstProduct";
import {EventService} from "../../service/event-service";

@Component({
    selector: 'app-documents-and-books-tab',
    templateUrl: './documents-and-books-tab.component.html',
    styleUrls: ['./documents-and-books-tab.component.css']
})
export class DocumentsAndBooksTabComponent implements OnInit, AfterViewInit {

    private chooseTypeMenuDocAndBook: string = "";
    private typeChooseDocumentAndBooks: string;
    private isOpenDocAndBookMenu:boolean = false;

    private SIMPLE_FACEBOOK = ConstProductKey.SIMPLE_FACEBOOK;
    private SIMPLE_ACCOUNT = ConstProductKey.SIMPLE_ACCOUNT;
    private SIMPLE_ZALO = ConstProductKey.SIMPLE_ZALO;
    private SIMPLE_SEEDING_MOBILE = "SimpleSeedingMobile";
    private SIMPLE_SEEDING = ConstProductKey.SIMPLE_SEEDING;
    private SIMPLE_INSTAGRAM = ConstProductKey.SIMPLE_INSTAGRAM;
    private SIMPLE_UID = ConstProductKey.SIMPLE_UID;
    private AUTOVIRAL_CONTENT = ConstProductKey.AUTOVIRAL_CONTENT;
    private SIMPLE_ADS = ConstProductKey.SIMPLE_ADS;
    private CRM_PROFILE = ConstProductKey.CRM_PROFILE;
    private ATP_CARE = ConstProductKey.ATP_CARE;
    private SIMPLE_LIVESTREAM = ConstProductKey.SIMPLE_LIVESTREAM;
    //private SIMPLE_FANPAGE = ConstProductKey.SIMPLE_FANPAGE;

    private IS_SIMPLE_FACEBOOK = false;
    private IS_SIMPLE_ACCOUNT = false;
    private IS_SIMPLE_ZALO = false;
    private IS_SIMPLE_SEEDING_MOBILE = false;
    private IS_SIMPLE_SEEDING = false;
    private IS_SIMPLE_INSTAGRAM = false;
    private IS_SIMPLE_UID = false;
    private IS_AUTOVIRAL_CONTENT = false;
    private IS_SIMPLE_ADS = false;
    private IS_CRM_PROFILE = false;
    private IS_ATP_CARE = false;
    private IS_SIMPLE_LIVESTREAM = false;
    //private IS_SIMPLE_FANPAGE = false;


    private BOOK: string = "BOOK";
    private DOCUMENTS: string = "DOCUMENTS";

    constructor(private eventService: EventService) {

    }

    ngOnInit() {
        let tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        let firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    ngAfterViewInit(): void {
        this.typeChooseDocumentAndBooks = "DOCUMENTS";
        this.eventService.sendTypeDocAndBook.subscribe((typeDocAndBook) => {
            this.typeChooseDocumentAndBooks = typeDocAndBook;
        })
    }

    // li - 1
    private clickSimpleFacebook() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_FACEBOOK = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_FACEBOOK;
    }

    // li - 2
    private clickSimpleAccount() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_ACCOUNT = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_ACCOUNT;
    }

    // li - 3
    private clickSimpleZalo() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_ZALO = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_ZALO;
    }

    // li - 4
    private clickSimpleSeedingMobile() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_SEEDING_MOBILE = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_SEEDING_MOBILE;
    }

    // li - 4
    private clickSimpleSeeding() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_SEEDING = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_SEEDING;
    }

    // li - 4
    private clickSimpleInstagram() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_INSTAGRAM = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_INSTAGRAM;
    }

    // li - 4
    private clickSimpleUID() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_UID = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_UID;
    }

    // li - 4
    private clickAutoViralContent() {
        this.isOpenDocAndBookMenu = true;
        this.IS_AUTOVIRAL_CONTENT = true;
        return this.chooseTypeMenuDocAndBook = this.AUTOVIRAL_CONTENT;
    }

    // li - 4
    private clickSimpleAds() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_ADS = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_ADS;
    }

    // li - 4
    private clickCRMProFile() {
        this.isOpenDocAndBookMenu = true;
        this.IS_CRM_PROFILE = true;
        return this.chooseTypeMenuDocAndBook = this.CRM_PROFILE;
    }

    // li - 4
    private clickATPCare() {
        this.isOpenDocAndBookMenu = true;
        this.IS_ATP_CARE = true;
        return this.chooseTypeMenuDocAndBook = this.ATP_CARE;
    }

    // li - 4
    private clickSimpleLivestream() {
        this.isOpenDocAndBookMenu = true;
        this.IS_SIMPLE_LIVESTREAM = true;
        return this.chooseTypeMenuDocAndBook = this.SIMPLE_LIVESTREAM;
    }

    // private clickSimpleFanpage() {
    //     this.isOpenDocAndBookMenu = true;
    //     this.IS_SIMPLE_FANPAGE = true;
    //     return this.chooseTypeMenuDocAndBook = this.SIMPLE_FANPAGE;
    // }


}
