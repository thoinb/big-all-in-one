import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-seeding',
    templateUrl: './documents-simple-seeding.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleSeedingComponent implements OnInit, AfterViewInit {

    private players: any = [];

    private playerInfoListSimpleSeeding: any = [{
        id: 'playerSimpleSeeding1', // Hướng dẫn sử dụng phần mềm Simple Seeding
        height: 'auto',
        width: 'auto',
        videoId: 'nDPPEn7IflI'
    }];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-seeding').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleSeeding === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleSeeding.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleSeeding[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/1lLjJXhdnZJPZmbgVoOOcvOOdtTjktcUV/view";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
