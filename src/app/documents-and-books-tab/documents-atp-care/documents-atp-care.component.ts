import { Component, OnInit } from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-atp-care',
    templateUrl: './documents-atp-care.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsAtpCareComponent implements OnInit {
    private players: any = new Array();

    private playerInfoListAtpCare: any = [
        {
            id: 'playerAtpCare1', //HHướng Dẫn Đăng Nhập Vào Phần Mềm
            height: 'auto',
            width: 'auto',
            videoId: '4Pt31cTghN8'
        }, {
            id: 'playerAtpCare2', //Hướng Dẫn Khai Thác Thống Kê Hoạt Động Của Fanpag
            height: 'auto',
            width: 'auto',
            videoId: 'gkLqOVS2CfI'
        }, {
            id: 'playerAtpCare3', //Hướng Dẫn Các Bước Cài Đặt Cơ Bản Trên Fanpage - ATP CARE
            height: 'auto',
            width: 'auto',
            videoId: 'rNXx__QuhXk'
        }, {
            id: 'playerAtpCare4', //Hướng Dẫn Chức Năng Tạo Đơn Hàng - ATP CARE
            height: 'auto',
            width: 'auto',
            videoId: 'Y238gDDNlUI'
        }, {
            id: 'playerAtpCare5', //Hướng Dẫn Quản Lí Bình Luận, Tin Nhắn Trên Fanpage - ATP CARE
            height: 'auto',
            width: 'auto',
            videoId: 'HqFnzym0MUk'
        }, {
            id: 'playerAtpCare6', //Hướng dẫn sử dụng chatbot, quản lý đơn hàng, quản lý khách hàng trên CRM Profilv
            height: 'auto',
            width: 'auto',
            videoId: 'kP8pnI0Jl_w'
        }
    ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-crm-pro-file').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady()
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListAtpCare === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListAtpCare.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListAtpCare[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://sum.vn/atpcare";
        (<any>window).require('electron').shell.openExternal(src);
    }

}
