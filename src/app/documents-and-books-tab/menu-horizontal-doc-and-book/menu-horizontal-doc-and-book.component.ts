import {Component, Input, OnInit} from '@angular/core';
import {ConstLogo, ConstLogoCode} from "../../../shared/const/ConstLogo";
import {ConstProductKey} from "../../../shared/const/ConstProduct";
import {EventService} from "../../../service/event-service";

@Component({
    selector: 'app-menu-horizontal-doc-and-book',
    templateUrl: './menu-horizontal-doc-and-book.component.html',
    styleUrls: ['./menu-horizontal-doc-and-book.component.css']
})
export class MenuHorizontalDocAndBookComponent implements OnInit {

    @Input() isOpenDocAndBookMenu: boolean = false;
    private typeDocAndBook: string = "";

    constructor(private eventService: EventService) {
    }

    ngOnInit() {

    }

    clickDOC() {
        this.typeDocAndBook = "DOCUMENTS";
        this.sendTypeDocAndBook(this.typeDocAndBook);
    }

    clickBOOK() {
        this.typeDocAndBook = "BOOK";
        this.sendTypeDocAndBook(this.typeDocAndBook);
    }

    private sendTypeDocAndBook(typeDocAndBook) {
        this.eventService.sendTypeDocAndBook.emit(typeDocAndBook);
    }

}
