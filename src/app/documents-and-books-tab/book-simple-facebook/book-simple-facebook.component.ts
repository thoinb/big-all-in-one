import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-book-simple-facebook',
    templateUrl: './book-simple-facebook.component.html',
    styleUrls: ['./book-simple-facebook.component.css']
})
export class BookSimpleFacebookComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/1n2E2_LRpCLZPH88IlJnn4ko015pfbJE3/view";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
