import {AfterViewInit, Component, OnInit} from '@angular/core';

declare let $: any;

@Component({
    selector: 'app-documents-simple-livestream',
    templateUrl: './documents-simple-livestream.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})

export class DocumentsSimpleLivestreamComponent implements OnInit ,AfterViewInit{

    private players: any = new Array();

    private playerInfoListSimpleLivestream: any = [
        {
            id: 'playerSimpleLivestream1', // HHướng dẫn tải cài đặt và đăng nhập Simple Livestream mới nhất 202
            height: 'auto',
            width: 'auto',
            videoId: 'bmGyTtZZZbc'
        }, {
            id: 'playerSimpleLivestream2', // Hướng dẫn tính năng phát lại Livestream của Simple Livestream mới nhất 2020
            height: 'auto',
            width: 'auto',
            videoId: 'geN3YUyVqgE'
        }, {
            id: 'playerSimpleLivestream3', // Hướng dẫn tạo sản phẩm Simple Livestream mới nhất 2020
            height: 'auto',
            width: 'auto',
            videoId: 'X8lRrIq5wp0'
        }, {
            id: 'playerSimpleLivestream4', // Hướng dẫn tính năng quản lí livestream và quản lí đơn hàng
            height: 'auto',
            width: 'auto',
            videoId: 'HZzYL7dLpLs'
        },
    ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-livestream').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleLivestream === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleLivestream.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleLivestream[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://sum.vn/67nV4";
        (<any>window).require('electron').shell.openExternal(src);
    }

}
