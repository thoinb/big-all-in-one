import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-instagram',
    templateUrl: './documents-simple-instagram.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleInstagramComponent implements OnInit, AfterViewInit {

    private players: any = [];

    private playerInfoListSimpleInstagram: any = [{
            id: 'playerSimpleInstagram1', // Hướng dẫn sử dụng phần mềm Simple instagram
            height: 'auto',
            width: 'auto',
            videoId: 'lGBNQ7YYm3k'
        },
        {
            id: 'playerSimpleInstagram2', // Hướng dẫn sử dụng phần mềm Simple instagram
            height: 'auto',
            width: 'auto',
            videoId: 'DicQh9h7c7k'
        },
        {
            id: 'playerSimpleInstagram3', // Hướng dẫn sử dụng phần mềm Simple instagram
            height: 'auto',
            width: 'auto',
            videoId: '5fkONS8AhLg'
        },
        {
            id: 'playerSimpleInstagram4', // Hướng dẫn sử dụng phần mềm Simple instagram
            height: 'auto',
            width: 'auto',
            videoId: 'ImbLIiy234w'
        },
    ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-instagram').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleInstagram === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleInstagram.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleInstagram[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://drive.google.com/file/d/11NpA2f-B5NRr6xBNsFdTxLrqWj6jgWuG/view";
        (<any>window).require('electron').shell.openExternal(src);
    }
}
