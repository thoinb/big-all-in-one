import {AfterViewInit, Component, OnInit} from '@angular/core';
declare let $: any;
@Component({
    selector: 'app-documents-simple-fanpage',
    templateUrl: './documents-simple-fanpage.component.html',
    styleUrls: ['./../documents-and-books-tab.component.css']
})
export class DocumentsSimpleFanpageComponent implements OnInit ,AfterViewInit{

    private players: any = new Array();

    private playerInfoListSimpleFanpage: any = [
        {
            id: 'playerSimpleFanpage1', //Video hướng dẫn đăng nhậ
            height: 'auto',
            width: 'auto',
            videoId: 'nhjo_OH7oLw'
        }
    ];

    constructor() {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        $('#stop-simple-fanpage').click(() => {
            $(this.players).each(function (i) {
                console.log(this);
                this.stopVideo();
            });
        });
        this.onYouTubeIframeAPIReady();
    }

    onYouTubeIframeAPIReady() {
        if (typeof this.playerInfoListSimpleFanpage === 'undefined')
            return;
        for (let i = 0; i < this.playerInfoListSimpleFanpage.length; i++) {
            this.players[i] = this.createPlayer(this.playerInfoListSimpleFanpage[i]);
        }
    }

    createPlayer(playerInfo) {
        return new YT.Player(playerInfo.id, {
            height: playerInfo.height,
            width: playerInfo.width,
            videoId: playerInfo.videoId,
        });
    }

    clickDownloadPDF1() {

    }

    clickViewPDF1() {
        let src = "https://sum.vn/autoviral"; // chuaw co
        (<any>window).require('electron').shell.openExternal(src);
    }

}
