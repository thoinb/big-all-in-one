import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-lst-sofeware-tab',
    templateUrl: './lst-sofeware-tab.component.html',
    styleUrls: ['./lst-sofeware-tab.component.css']
})
export class LstSofewareTabComponent implements OnInit {

    private opacityTurnOn: boolean = true;

    constructor() {
    }

    ngOnInit() {
    }

    private clickALLINONE() {
        return this.opacityTurnOn = true;
    }

    private clickBIGALLINONE() {
        return this.opacityTurnOn = false;
    }
}
