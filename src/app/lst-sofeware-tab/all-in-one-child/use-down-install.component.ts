import {AfterViewInit, ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {EventService} from "../../../service/event-service";
import {FileProcessing} from "../../../shared/request/file-processing";
import {ProductStatus} from "../../../shared/enum/ProductStatus";
import {ConstProductKey} from "../../../shared/const/ConstProduct";
import {TypeTool} from "../../../shared/enum/TypeTool";
import {Constant} from "../../../shared/constrant/Constant";
import {Utils} from "../../../shared/utils/utils";

declare let $: any;

@Component({
    selector: 'app-use-down-install',
    template: `
        <div *ngIf="productRender && productRender.status === productStatus.NOT_FILE_INSTALL ;else notNOT_FILE_INSTALL">
            <div *ngIf="disabled === true;else DownAndInstall">
                <div class="content-button-not-installed-background">
                    <span class="content-button-not-installed">Tải và cài đặt</span>
                </div>
            </div>
            <ng-template #DownAndInstall>
                <div class="content-button-not-installed-background" (click)="clickDownAndInstall(productKey)">
                    <span class="content-button-not-installed">Tải và cài đặt</span>
                </div>
            </ng-template>
        </div>

        <ng-template #notNOT_FILE_INSTALL>
            <div *ngIf="productRender && productRender.status === productStatus.DOWNLOADING_FILE_INSTALL ;else notDOWNLOADING_FILE_INSTALL">
                <div class="content-button-loading-background">
                    <div *ngIf="disabled;else CancelDownload">
                        <span aria-hidden="true" class="spinner-grow spinner-grow-sm " role="status"
                              style="margin-left: 5px;"></span>
                        <span style="margin-left: 5px" class="loadingText{{productKey}}">Đang tải...</span>
                        <button class="danger-cancel danger-cancel{{productKey}}"> Hủy</button>
                    </div>
                </div>
                <ng-template #CancelDownload>
                    <span aria-hidden="true" class="spinner-grow spinner-grow-sm " role="status"
                          style="margin-left: 5px;"></span>
                    <span style="margin-left: 5px" class="loadingText{{productKey}}">Đang tải...</span>
                    <button class="danger-cancel danger-cancel{{productKey}}" (click)="clickCancelDownload(productKey)">
                        Hủy
                    </button>
                </ng-template>
                <div class="progress progress-edit">
                    <div class="progress-bar progress-bar-striped progress-bar-animated progress{{productKey}}"></div>
                </div>
            </div>
        </ng-template>

        <ng-template #notDOWNLOADING_FILE_INSTALL>
            <div *ngIf="productRender && productRender.status === productStatus.DOWNLOADED_FILE_INSTALL ;else notDOWNLOADED_FILE_INSTALL">
                <div *ngIf="disabled;else install">
                    <div class="content-button-use-background">
                        <span class="content-button-use content-button-use{{productKey}}">Cài đặt</span>
                    </div>
                </div>
                <ng-template #install>
                    <div class="content-button-use-background" (click)="clickInstall(productKey)">
                        <span class="content-button-use content-button-use{{productKey}}">Cài đặt</span>
                    </div>
                </ng-template>
            </div>
        </ng-template>

        <ng-template #notDOWNLOADED_FILE_INSTALL>
            <div *ngIf="productRender && (productRender.status === productStatus.HAS_BEEN_FILE_INSTALLED || productRender.status === productStatus.COPYRIGHT_IS_STILL_VALID);else notCOPYRIGHT_IS_STILL_VALID">
                <div *ngIf="disabled;else use">
                    <div class="content-button-use-background" >
                        <span class="content-button-use content-button-use{{productKey}}">Sử dụng</span>
                    </div>
                </div>
                <ng-template #use>
                    <div class="content-button-use-background" (click)="clickUse(productKey)">
                        <span class="content-button-use content-button-use{{productKey}}">Sử dụng</span>
                    </div>
                </ng-template>
            </div>
        </ng-template>
        <ng-template #notCOPYRIGHT_IS_STILL_VALID>
            <div *ngIf="productRender && productRender.status === productStatus.COPYRIGHT_HAS_EXPIRED;else order">
                <div class="content-button-use-background-expired">
                    <span class="content-button-use-expired content-button-use{{productKey}}">Hết hạn</span>
                </div>
            </div>
        </ng-template>

        <ng-template #order>
            <div class="content-button-not-installed-background">
                <span class="content-button-not-installed">Đang quét</span>
            </div>
        </ng-template>
    `,
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./all-in-one-child.component.css']
})

export class UseDownInstallComponent implements OnInit, AfterViewInit, OnDestroy {

    private file: FileProcessing = new FileProcessing();
    private productStatus: typeof ProductStatus = ProductStatus;

    @Input() disabled:boolean;
    @Input() productKey: string;
    @Input() opacityTurnOn: boolean = true;

    private productRender: any;
    private appLstMapProductRenderToButtonAllInOne: any;

    constructor(private eventService: EventService, private ref: ChangeDetectorRef) {
    }

    ngOnInit() {

    }

    ngAfterViewInit(): void {
        if (this.disabled) {
            this.disabled = true;
        } else {
            this.disabled = false;
        }

        this.appLstMapProductRenderToButtonAllInOne = this.eventService.appLstMapProductRenderToButtonAllInOne.subscribe((lstMapProductRender) => {
            this.productRender = lstMapProductRender.get(this.productKey);
        });

        this.eventService.appProductRenderToButtonAllInOne.subscribe((productRender) => {
            if (this.productKey === productRender.productKey) {
                this.productRender.status = productRender.status;
                if (productRender.status === this.productStatus.HAS_BEEN_FILE_INSTALLED) {
                    this.productRender.file_open = productRender.file_open;
                    this.ref.detectChanges()
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.appLstMapProductRenderToButtonAllInOne.unsubscribe();
    }

    public clickDownAndInstall(productKey) {
        if (Constant.isUser) {
            if (productKey) {
                this.productRender.status = this.productStatus.DOWNLOADING_FILE_INSTALL;
                this.ref.detectChanges();
                this.file.downloadFile(this.productRender,
                    (productKey, progress) => {
                        this.productRender.status = this.productStatus.DOWNLOADING_FILE_INSTALL;
                        this.productRender.progress = progress + '%';
                        if (productKey === ConstProductKey.ATP_SPLITTER) {
                            $('.progressATP').css("width", progress + '%');
                            $('.content-titleATP-SPLITTER').text("ATP SPLITTER " + parseInt(progress) + '%');
                        } else {
                            $('.progress' + productKey).css("width", progress + '%');
                            $('.content-title' + productKey).text(this.productRender.productName + " " + parseInt(progress) + '%');
                        }
                    },
                    (productKey, pathSave) => {

                        if (productKey === ConstProductKey.ATP_SPLITTER) {
                            this.productRender.status = this.productStatus.HAS_BEEN_FILE_INSTALLED;
                            this.productRender.file_open = pathSave + this.productRender.file_name;
                            $('.content-titleATP-SPLITTER').text('ATP SPLITTER');
                            this.ref.detectChanges();
                            /*$('.content-button-use' + productKey).text("Đang mở ... ");*/
                            /* setTimeout(() => {*/

                            /* this.file.openFile(product.file_install);*/
                            $('.content-button-useATP').text("Sử dụng");

                        } else {
                            this.productRender.status = this.productStatus.DOWNLOADED_FILE_INSTALL;
                            this.productRender.file_install = pathSave + this.productRender.file_name;
                            $('.content-title' + productKey).text(this.productRender.productName);
                            this.ref.detectChanges();
                            /*$('.content-button-use' + productKey).text("Đang mở ... ");*/
                            /* setTimeout(() => {*/

                            /* this.file.openFile(product.file_install);*/
                            $('.content-button-use' + productKey).text("Cài đặt")
                            /* }, 1000);*/
                        }

                    }, (productKey, error) => {
                        alert("download: " + productKey + " : " + error);
                        this.productRender.status = this.productStatus.NOT_FILE_INSTALL;
                        /*this.ref.detectChanges();*/
                    }
                );
            }
        } else { Utils.messageDis();}
    }

    public clickCancelDownload(productKey) {
        if (Constant.isUser) {
            if (productKey) {
                this.file.cancelDownloadFile(this.productRender,
                    (productKey) => {
                        /*  $('.danger-cancel' + productKey).attr('disabled', 'disabled');
                          $('.danger-cancel' + productKey).addClass('disabled');
                          $('.loadingText' + productKey).text("Đang hủy!!!! ");*/
                        /*setTimeout(() => {*/
                        $('.content-title' + productKey).text(this.productRender.productName);
                        this.productRender.status = this.productStatus.NOT_FILE_INSTALL;

                        /*this.eventService.lstMapProductRenderAllToBigAll.emit(this.lstMapProductRender);*/
                        this.ref.detectChanges();

                        $('.danger-cancel' + productKey).removeAttr('disabled');
                        $('.danger-cancel' + productKey).removeClass('disabled');
                        $('.loadingText' + productKey).text("Đang tải...");
                        /*}, 1000);*/
                    },
                    (productKey, error) => {
                        this.productRender.status = this.productStatus.NOT_FILE_INSTALL;
                    }
                );
            }
        } else { Utils.messageDis();}

    }

    public clickInstall(productKey) {
        if (Constant.isUser) {
            if (productKey) {
                this.file.openFile(this.productRender.file_install);
            }
        } else {
            Utils.messageDis();
        }
    }

    public clickUse(productKey) {
        if (Constant.isUser) {
            if (productKey) {
                if (this.productRender.typeTool === TypeTool.WEB) {
                    (<any>window).require('electron').shell.openExternal(this.productRender.file_url_64)
                } else {
                    this.file.openFile(this.productRender.file_open);
                }
            }
        } else {
            Utils.messageDis();
        }
    }
}
