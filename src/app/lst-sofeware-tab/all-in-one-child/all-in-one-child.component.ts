import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ConstProductKey} from "../../../shared/const/ConstProduct";
declare let $: any;
@Component({
    selector: 'app-all-in-one-child',
    templateUrl: './all-in-one-child.component.html',
    styleUrls: ['./all-in-one-child.component.css']
})
export class AllInOneChildComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() opacityTurnOn: boolean = true;
    @Input() disbaled:boolean = true;
    private SIMPLE_FACEBOOK: string = ConstProductKey.SIMPLE_FACEBOOK;
    private SIMPLE_ACCOUNT: string = ConstProductKey.SIMPLE_ACCOUNT;
    private SIMPLE_INSTAGRAM: string = ConstProductKey.SIMPLE_INSTAGRAM;
    private SIMPLE_ZALO: string = ConstProductKey.SIMPLE_ZALO;
    private SIMPLE_FACEBOOK_PRO: string = ConstProductKey.SIMPLE_FACEBOOK_PRO;
    private SIMPLE_TIKDOWN: string = ConstProductKey.SIMPLE_TIKDOWN;
    private AUTOVIRAL_CONTENT: string = ConstProductKey.AUTOVIRAL_CONTENT;
    private SIMPLE_ADS: string = ConstProductKey.SIMPLE_ADS;
    private SIMPLE_UID: string = ConstProductKey.SIMPLE_UID;
    private SIMPLE_SEEDING: string = ConstProductKey.SIMPLE_SEEDING;
    private ATP_SEO: string = ConstProductKey.ATP_SEO;
    private CRM_PROFILE: string = ConstProductKey.CRM_PROFILE;
    private SIMPLE_TIKTOK: string = ConstProductKey.SIMPLE_TIKTOK;
    private ATP_CARE: string = ConstProductKey.ATP_CARE;
    private SIMPLE_LIVESTREAM: string = ConstProductKey.SIMPLE_LIVESTREAM;
    // private SIMPLE_FANPAGE: string = ConstProductKey.SIMPLE_FANPAGE;
    private SIMPLE_PAGE: string = ConstProductKey.SIMPLE_PAGE;
    private SIMPLE_SHOP: string = ConstProductKey.SIMPLE_SHOP;
    private ATP_LINK: string = ConstProductKey.ATP_LINK;
    private SIMPLE_MARKETPLACE: string = ConstProductKey.SIMPLE_MARKETPLACE;
    private FACEBOOK_ATP_SYSTEM: string = ConstProductKey.FACEBOOK_ATP_SYSTEM;
    private SIMPLE_NINJA_PRO: string = ConstProductKey.SIMPLE_NINJA_PRO;

    constructor() {

    }

    ngOnInit() {
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {

    }

}
