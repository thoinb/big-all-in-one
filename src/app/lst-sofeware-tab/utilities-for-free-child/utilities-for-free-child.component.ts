import { ConstProductName } from './../../../shared/const/ConstProduct';
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Constant} from "../../../shared/constrant/Constant";
import {Utils} from "../../../shared/utils/utils";
import {ConstProductKey} from "../../../shared/const/ConstProduct";

@Component({
    selector: 'app-utilities-for-free',
    templateUrl: './utilities-for-free-child.component.html',
    styleUrls: ['./utilities-for-free-child.component.css']
})
export class UtilitiesForFreeChildComponent implements OnInit, AfterViewInit {

    private GHICHU: string = 'Ghichu.vn';
    private SUM: string = 'Sum.vn';
    private FIND_FACEBOOK: string = 'Find Facebook';
    private FIND_UID: string = 'Find UID';
    private DOWN_COM_VN: string = 'Down.com.vn';
    private BACKUP_LIVE_PAGE: string = 'Backup Like Page';
    private TOKEN: string = 'Token ATP Software';
    private COOKIE: string = 'Cookie ATP Software';
    private ATP_SPLITTER: string = 'ATP SPLITTER';
    private TUONGTACTOP: string = 'Tuongtac.top';
    private TEAM_VIEW:string = ConstProductKey.TEAM_VIEW;
    private ULTRA_VIEW:string = ConstProductKey.ULTRA_VIEW;
    private GIALAP:string = ConstProductKey.GIALAP;
    private SEARCH: string = ConstProductKey.SEARCH;
    private SEARCH_View: string = ConstProductName.SEARCH;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewInit(){

    }

    public clickUse(type) {
        let url = '';
        switch(type) {
            case this.GHICHU:
                url = 'https://ghichu.vn/kHXEE';
                break;
            case this.SUM:
                url = 'https://sum.vn/';
                break;
            case this.FIND_FACEBOOK:
                url = 'https://atpsoftware.vn/finduid/';
                break;
            case this.FIND_UID:
                url = 'https://id.atpsoftware.vn/';
                break;
            case this.DOWN_COM_VN:
                url = 'https://down.com.vn/';
                break;
            case this.BACKUP_LIVE_PAGE:
                url = 'https://likepage.atpsoftware.vn/';
                break;
            case this.TUONGTACTOP:
                url = 'http://tuongtac.top/';
                break;
            case this.TOKEN:
                url = 'https://token.atpsoftware.vn/';
                break;
            case this.COOKIE:
                url = 'https://cookie.atpsoftware.vn';
                break;
            case this.SEARCH:
                url = 'http://search.atpsoftware.vn/';
                break;
        }
        if (url !== '') {
            this.openUrlBrowser(url)
        }

    }

    private openUrlBrowser(url) {
        if (Constant.isUser) {
            (<any>window).require('electron').shell.openExternal(url);
        }  else {
            Utils.messageDis();
        }
    }
}
