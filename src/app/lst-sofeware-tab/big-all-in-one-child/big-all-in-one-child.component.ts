import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';

@Component({
    selector: 'app-big-all-in-one-child',
    templateUrl: './big-all-in-one-child.component.html',
    styleUrls: ['./big-all-in-one-child.component.css']
})
export class BigAllInOneChildComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() opacityTurnOn: boolean = false;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {

    }

}
