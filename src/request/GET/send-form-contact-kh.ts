import {PostGetRequest} from "../../shared/request/post-get-request";

export default class SendFormContactKh extends PostGetRequest {

    public name: string;
    public phone: string;
    public date: string;
    private nameProduct: string;

    constructor(name: string, phone: string, date: string,  nameProduct: string) {
        super();
        this.name = name;
        this.phone = phone;
        this.date = date;
        this.nameProduct = nameProduct;
    }

    async task(): Promise<any> {
      this.url = 'http://crm.atpsoftware.vn/api-hethan.php?name=' + this.name + '&phone=' + this.phone + '&nameproduct=' + this.nameProduct + '&dateOfBirth' + this.date;
      await this.sendGet();
      let result = this.result;
    }

}
