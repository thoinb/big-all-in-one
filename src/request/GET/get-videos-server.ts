import {PostGetRequest} from "../../shared/request/post-get-request";
import { resolve } from "url";
import { Utils } from "src/shared/utils/utils";
import {Video} from "../../shared/models/Video";

export class GetVideos extends PostGetRequest {

    private _getData: any;
    private id:number;
    private _arrVideos: Array<Video> = new Array<Video>();

    constructor(id:number) {
        super();
        this.id=id;
    }

    async task(): Promise<any>{
        try {
            let key = await Utils.getKey();
            // this.url = "https://atpacademy.vn/api/?getData=listVideos&keyBIG=ATP-BIG-1876-1224-C100-A976-C632-B72E-6825-87231&idCourse="+this.id;
            // this.url = "http://v1.atpacademy.vn/api/?getData=listVideos&keyBIG="+ key +"&idCourse=" + this.id;
            // https://atpacademy.vn/api/detail_course/12/ATP-BIG-1827-123-123-123123-123-12-3123-123-
            this.url = "https://atpacademy.vn/api/detail_course/"+this.id+'/'+key
            await this.sendGet();
            let result = this.result ? JSON.parse(this.result) : null;
            if (result) {
                result.lessons.forEach(lessons => {
                    if (lessons && lessons.video_url) {
                        let video = new Video();
                        video.title = lessons.title;
                        video.checkTitle = 1;
                        video.link = lessons.video_url;
                        let str1: string[];
                        let str2: string[];
                        str1 = lessons.video_url.split("/")
                        let str1Len = str1.length;
                        str2 = str1[str1Len - 1].split("=");
                        let str2Len = str2.length;
                        video.idVideo = str2[str2Len - 1];
                        video.linkImg = "https://img.youtube.com/vi/" + video.idVideo + "/default.jpg";
                        this._arrVideos.push(video);
                    }
                })
            }
        } catch (e) {
            throw new Error(e)
        }
    };


    get arrVideos(): Array<Video> {
        return this._arrVideos;
    }

    get getData(): any {
        return this._getData;
    }

    set getData(value: any) {
        this._getData = value;
    }
}
