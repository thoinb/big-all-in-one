import {PostGetRequest} from "../../shared/request/post-get-request";
import {Course} from "../../shared/models/course";
import {Constant} from "../../shared/constrant/Constant";
import {Utils} from "../../shared/utils/utils";

export class GetDataCourse extends PostGetRequest {

    private _lstCourse: Array<Course> = new Array<Course>();

    constructor() {
        super();
    }

    async task(): Promise<any>{
        // this.url = "http://v1.atpacademy.vn/api/?getData=listCourses";
        try {
            this.url = "https://atpacademy.vn/api/top_courses";
            await this.sendGet();
            let result = this.result.length > 0 ? JSON.parse(this.result) : null;
            if(result) {
                for (let cos of result) {
                    let typeCourse = Constant.typeCourse.PAID_COURSE;
                    if (Utils.changeAlias(cos.price.toLowerCase())=== Utils.changeAlias("Miễn phí".toLowerCase())) {
                        typeCourse = Constant.typeCourse.FREE_COURSE;
                    }
                    let course = new Course(cos.id, cos.thumbnail, cos.title, typeCourse);
                    this._lstCourse.push(course)
                }
            }
        } catch (e) {
            throw new Error("GetDataCourse :"+ e)
        }
    }

    get lstCourse(): Array<Course> {
        return this._lstCourse;
    }

}