import {PostGetRequest} from "../../shared/request/post-get-request";
import {Key} from "../../shared/constrant/Key";
import {Utils} from "../../shared/utils/utils";

export class GetKeyAndStatusSoftServer extends PostGetRequest {

    private _getData: any;
    private readonly keyRegistry: string;
    constructor(keyRegistry:string) {
        super();
        this.keyRegistry = keyRegistry;
    }

    async task(): Promise<any> {
        try {
            this.url = Key.getKeyAndStatusDate(this.keyRegistry);
            await this.sendGet();
            let indexOf = null;
            if (this.result) {
                indexOf = this.result.indexOf("|");
            }
            this.getData = this.result.substring(0, indexOf);
        } catch (e) {
            throw new Error("GetKeyAndStatusSoftServer :" + Utils.getError(e));
        }

    }

    get getData(): any {
        return this._getData;
    }

    set getData(value: any) {
        this._getData = value;
    }
}