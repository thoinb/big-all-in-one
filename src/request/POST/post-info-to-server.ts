import {PostGetRequest} from "../../shared/request/post-get-request";

export class PostInfoToServer extends PostGetRequest {

    private param:any;

    constructor(param) {
        super();
        this.param = param;
    }

    async task(): Promise<any> {
        this.url = 'http://tool.puno2.com/aio_form.php?nameATP='+this.param.nameATP +'&phoneATP=' + this.param.phoneATP + '&genderATP=' + this.param.key + '&emailATP=' + this.param.emailATP + '&key=' + this.param.key;
        await this.sendGet();
        let result = this.result;
    }

}
