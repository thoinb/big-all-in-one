export enum PlatformsWindows {
    WINDOWS_32 = "Win32",
    WINDOWS_64 = "Win64",
    WINDOWS = "Windows",
    WIN_CE = "WinCE"
}