export enum ProductStatus {
     NOT_FILE_INSTALL = 0, // not file install
     DOWNLOADING_FILE_INSTALL = 1, // downloading file install
     DOWNLOADED_FILE_INSTALL = 2, // downloaded file install
     HAS_BEEN_FILE_INSTALLED = 3, // Software has been installed
     COPYRIGHT_HAS_EXPIRED = 4, // Copyright has expired
     COPYRIGHT_IS_STILL_VALID = 5, // Copyright is still valid
}