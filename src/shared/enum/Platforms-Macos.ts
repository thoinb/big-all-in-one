export enum MacosPlatforms { // window.navigator.platform
    MAC_INTOSH = "Macintosh",
    MAC_INTEL = "MacIntel",
    MAC_PPC = "MacPPC",
    MAC_68K = "Mac68K",
    MAC_OS = "MAC_OS"
}