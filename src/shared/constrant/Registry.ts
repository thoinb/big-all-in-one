export class Registry {
    public static get HKCU_Software_ATP(): string {
        return 'HKCU\\Software\\ATP\\';
    }

    public static get HKLM_Wow6432Node_Uninstall(): string {
        return 'HKLM\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\';
    }

    public static get HKLM_Uninstall(): string {
        return 'HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\';
    }
}