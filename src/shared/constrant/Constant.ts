export class Constant {
    static user_agent: string = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like.ts Gecko) Chrome/67.0.3396.99 Safari/537.36';
    public static textURLCheckVersion = "http://tool.puno2.com/simple_lic.php?hdd=";
    public static textURLCheckExpire: string = "http://tool.puno2.com/simple_exp.php?hdd=";
    public static readonly HOST_NAME_API = 'http://localhost:8080';
    public static versionApp = '2.4';
    public static isUser = false;
    public static typeCourse : { [key: string]: number } = {
        FREE_COURSE: 0,       // khóa học miễn phí
        PAID_COURSE: 1,    // khóa học có phí
    };

    public static statusRequest : { [key: string]: string } = {
        ERROR :'error',
        SUCCESS: 'success',
    };
    public static TOKEN_FCM: string = '';
}
