export class Course {
    private _idCourse: number;
    private _imageCourse: string;
    private _nameCourse: string;
    private _typeCourse: number;

    constructor(idCourse: number, imageCourse: string, nameCourse: string, typeCourse: number) {
        this._idCourse = idCourse;
        this._imageCourse = imageCourse;
        this._nameCourse = nameCourse;
        this._typeCourse = typeCourse;
    }

    get idCourse(): number {
        return this._idCourse;
    }

    set idCourse(value: number) {
        this._idCourse = value;
    }

    get imageCourse(): string {
        return this._imageCourse;
    }

    set imageCourse(value: string) {
        this._imageCourse = value;
    }

    get nameCourse(): string {
        return this._nameCourse;
    }

    set nameCourse(value: string) {
        this._nameCourse = value;
    }

    get typeCourse(): number {
        return this._typeCourse;
    }

    set typeCourse(value: number) {
        this._typeCourse = value;
    }
}

