export class NewsUpdate {
    private _contentUpdate: string;
    private _timestamp: number;
    private _stringTimeRelease: string;
    private _seen = false;

    constructor(contentUpdate: string, timestamp: number, stringTimeRelease: string) {
        this._contentUpdate = contentUpdate;
        this._timestamp = timestamp;
        this._stringTimeRelease = stringTimeRelease;
    }

    get seen(): boolean {
        return this._seen;
    }

    set seen(value: boolean) {
        this._seen = value;
    }

    get contentUpdate(): string {
        return this._contentUpdate;
    }

    set contentUpdate(value: string) {
        this._contentUpdate = value;
    }

    get timestamp(): number {
        return this._timestamp;
    }

    set timestamp(value: number) {
        this._timestamp = value;
    }

    get stringTimeRelease(): string {
        return this._stringTimeRelease;
    }

    set stringTimeRelease(value: string) {
        this._stringTimeRelease = value;
    }
}
