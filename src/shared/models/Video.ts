export class Video {
    link: string;
    linkImg: string;
    title: string;
    checkTitle: number;
    idVideo: string;
}