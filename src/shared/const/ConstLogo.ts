export const ConstLogoCode = {
    ATP_SOFTWARE: "ATP_SOFTWARE",
    ATP_ACADEMY: "ATP_ACADEMY",
    ATP_WEB: "ATP_WEB",
    CV_COM_VN: "CV_COM_VN",
    ATP_CARE: "ATP_CARE",
    ATP_LAND: "ATP_LAND",
    ATP_MEDIA: "ATP_MEDIA",
    VOZ_VN: "VOZ_VN",
    WINERP: "WINERP"
};

export const ConstLogoURL = {
    ATP_SOFTWARE: "assets/IMAGE/logo/ATP_SOFTWARE.png",
    ATP_ACADEMY: "assets/IMAGE/logo/ATP_ACADEMY.png",
    ATP_WEB: "assets/IMAGE/logo/ATP_WEB.png",
    CV_COM_VN: "assets/IMAGE/logo/CV_COM_VN.png",
    ATP_CARE: "assets/IMAGE/logo/ATP_CARE.png",
    ATP_LAND: "assets/IMAGE/logo/ATP_LAND.png",
    ATP_MEDIA: "assets/IMAGE/logo/ATP_MEDIA.png",
    VOZ_VN: "assets/IMAGE/logo/VOZ_VN.png",
    WINERP: "assets/IMAGE/logo/WINERP.png",
};

export const ConstLogo = new Map ([

    [ConstLogoCode.ATP_SOFTWARE, {
        url_img: ConstLogoURL.ATP_SOFTWARE,
        code: ConstLogoCode.ATP_SOFTWARE,
    }],

    [ConstLogoCode.ATP_ACADEMY, {
        url_img: ConstLogoURL.ATP_ACADEMY,
        code: ConstLogoCode.ATP_ACADEMY,
    }],

    [ConstLogoCode.ATP_WEB, {
        url_img: ConstLogoURL.ATP_WEB,
        code: ConstLogoCode.ATP_WEB,
    }],

    [ConstLogoCode.CV_COM_VN, {
        url_img: ConstLogoURL.CV_COM_VN,
        code: ConstLogoCode.CV_COM_VN,
    }],

    [ConstLogoCode.ATP_CARE, {
        url_img: ConstLogoURL.ATP_CARE,
        code: ConstLogoCode.ATP_CARE,
    }],

    [ConstLogoCode.ATP_LAND, {
        url_img: ConstLogoURL.ATP_LAND,
        code: ConstLogoCode.ATP_LAND,
    }],

    [ConstLogoCode.ATP_MEDIA, {
        url_img: ConstLogoURL.ATP_MEDIA,
        code: ConstLogoCode.ATP_MEDIA,
    }],

    [ConstLogoCode.VOZ_VN, {
        url_img: ConstLogoURL.VOZ_VN,
        code: ConstLogoCode.VOZ_VN,
    }],

    [ConstLogoCode.WINERP, {
        url_img: ConstLogoURL.WINERP,
        code: ConstLogoCode.WINERP,
    }],

]);