export const ConstProductKey = {
    SIMPLE_ACCOUNT: 'SIMPLE_ACCOUNT',
    SIMPLE_FACEBOOK: 'SIMPLE_FACEBOOK',
    SIMPLE_FACEBOOK_PRO: 'SIMPLE_FACEBOOK_PRO',
    SIMPLE_TIKDOWN: 'SIMPLE_TIKDOWN',
    SIMPLE_INSTAGRAM: 'SIMPLE_INSTAGRAM',
    SIMPLE_ZALO: 'SIMPLE_ZALO',
    AUTOVIRAL_CONTENT: 'AUTOVIRAL_CONTENT',
    SIMPLE_ADS: 'SIMPLE_ADS',
    SIMPLE_UID: 'SIMPLE_UID',
    SIMPLE_SEEDING: 'SIMPLE_SEEDING',
    ATP_SEO: 'ATP_SEO',
    CRM_PROFILE: 'CRM_PROFILE',
    SIMPLE_TIKTOK: 'SIMPLE_TIKTOK',
    ATP_CARE: 'ATP_CARE',
    SIMPLE_LIVESTREAM: 'SIMPLE_LIVESTREAM',
    ATP_SPLITTER: 'ATP SPLITTER',
    GIALAP: 'GIA_LAP',
    // SIMPLE_FANPAGE: 'SIMPLE_FANPAGE',
    TEAM_VIEW: 'TEAM_VIEW',
    ULTRA_VIEW: 'ULTRA_VIEW',
    SEARCH: 'SEARCH',
    SIMPLE_PAGE: 'SIMPLE_PAGE',
    SIMPLE_SHOP: 'SIMPLE_SHOP',
    ATP_LINK: 'ATP_LINK',
    SIMPLE_MARKETPLACE: 'ATP-MARKET',
    FACEBOOK_ATP_SYSTEM: 'FACEBOOK_ATP_SYSTEM',
    SIMPLE_NINJA_PRO: 'SIMPLE_NINJA_PRO'
};

export const ConstProductName = {
    SIMPLE_ACCOUNT: 'Simple Account',
    SIMPLE_FACEBOOK: 'Simple Facebook',
    SIMPLE_INSTAGRAM: 'SimpleInstagramV2',
    SIMPLE_ZALO: 'Simple Zalo',
    AUTOVIRAL_CONTENT: 'ATP-Content',
    SIMPLE_ADS: 'Simple Ads',
    SIMPLE_UID: 'Simple UID',
    SIMPLE_SEEDING: 'Simple Seeding',
    ATP_SEO: 'ATPSeo',
    SIMPLE_FACEBOOK_PRO: 'Simple Facebook Pro',
    //SIMPLE_TIKDOWN: 'SimpleTikDown',
    CRM_PROFILE: 'CRM Profile',
    SIMPLE_TIKTOK: 'Simple Tiktok',
    ATP_CARE: 'ATP Care',
    SIMPLE_LIVESTREAM: 'Simple Livestream',
    GIALAP: 'LDPlayer',
    // SIMPLE_FANPAGE: 'Simple Fanpage',
    TEAM_VIEW: 'TeamViewer',
    ULTRA_VIEW: 'UltraViewer',
    SEARCH: 'Simple Search 2020',
    SIMPLE_PAGE: 'Simple Page',
    SIMPLE_SHOP: 'Simple Shop',
    ATP_LINK: 'ATP Link',
    SIMPLE_MARKETPLACE: 'SimpleMarketPlace',
    FACEBOOK_ATP_SYSTEM: 'ATP Facebook System',
    SIMPLE_NINJA_PRO: 'Simple Ninja Pro'
};

export const ConstProductUrl64 = { // link 64
    SIMPLE_ACCOUNT: 'https://sum.vn/simpleaccv2', // done nef
    SIMPLE_FACEBOOK: 'https://sum.vn/spf_x64',// 'https://www.fshare.vn/file/8DZJHK2C8WLW', // done nef
    SIMPLE_FACEBOOK_PRO: 'https://atplink.com/sp_fb_pro_x64',// 'https://www.fshare.vn/file/8DZJHK2C8WLW', // done nef
    SIMPLE_TIKDOWN: 'https://atplink.com/stp_x64',// 'https://www.fshare.vn/file/8DZJHK2C8WLW', // done nef
    SIMPLE_INSTAGRAM: 'https://sum.vn/sp-ins-64', // done nef
    SIMPLE_ZALO: 'https://atplink.com/sp_zalo_v2_x64', // done
    AUTOVIRAL_CONTENT: 'https://atplink.com/autoviral-content-x64', // done
    SIMPLE_ADS: 'http://download.atpsoftware.link/SimpleAds2.exe', // done
    SIMPLE_UID: 'https://atplink.com/suid_x64',// https://www.fshare.vn/file/TGLADRHSCZO5', // done nef
    SIMPLE_SEEDING: 'https://download.atpsoftware.vn/s/cgxzPfiinTkTBJs/download',//'https://www.fshare.vn/file/NRMKODMPDGY6', // done
    ATP_SEO: 'https://download.atpsoftware.vn/s/NsstTJYWkx5J8M7/download',//'https://www.fshare.vn/file/NRMKODMPDGY6', // done
    CRM_PROFILE: 'https://sum.vn/new_crm_x64', // https://www.fshare.vn/file/DI61EKG8KUDX', // done
    SIMPLE_TIKTOK: 'http://download.atpsoftware.link/SimpleTiktok_x64.exe',
    ATP_CARE: 'https://simple.atpcare.vn/',
    SIMPLE_LIVESTREAM: 'https://sum.vn/livestream_x64', // done nef
    ATP_SPLITTER: 'https://sum.vn/atp_spliter_x64', // done nef
    // SIMPLE_FANPAGE:'http://uid.fanpage.vn',
    TEAM_VIEW: 'https://sum.vn/XIwt4',
    GIALAP: 'https://sum.vn/ldgialap',
    ULTRA_VIEW: 'https://sum.vn/ZUXyG',
    SEARCH: 'http://search.atpsoftware.vn',
    SIMPLE_PAGE: 'https://simplepage.vn/',
    SIMPLE_SHOP: 'https://simpleshop.vn/',
    ATP_LINK: 'https://atplink.com/',
    SIMPLE_MARKETPLACE: 'https://sum.vn/smp_64bit',
    FACEBOOK_ATP_SYSTEM: 'https://sum.vn/sfb_system',
    SIMPLE_NINJA_PRO: 'https://atplink.com/sp-ninja-x64'
};

export const ConstProductUrl32 = { // link 32
    SIMPLE_ACCOUNT: 'https://sum.vn/simpleaccount_v2_32bit', // done nef
    SIMPLE_FACEBOOK: 'https://sum.vn/spf_32', // ,https://www.fshare.vn/file/2MRLG7DZMVAG', // done
    SIMPLE_FACEBOOK_PRO: 'https://atplink.com/sp_fb_pro_x32', // ,https://www.fshare.vn/file/2MRLG7DZMVAG', // done
    SIMPLE_TIKDOWN: 'https://atplink.com/stp_x32', // ,https://www.fshare.vn/file/2MRLG7DZMVAG', // done
    SIMPLE_INSTAGRAM: 'https://sum.vn/sp-ins-32', // done nef
    SIMPLE_ZALO: 'https://atplink.com/sp_zalo_v2_x32', // chưa có bản 32 cho zalo
    AUTOVIRAL_CONTENT: 'https://atplink.com/autoviral-content-x32', // done
    SIMPLE_ADS: 'http://download.atpsoftware.link/SimpleAds2x32.exe', // done
    SIMPLE_UID: 'https://atplink.com/suid_x32',//'https://www.fshare.vn/file/3HUL6S7DW7XV', // done nef
    SIMPLE_SEEDING: 'https://download.atpsoftware.vn/s/aCkQyg25b32eSKw/download', //,https://www.fshare.vn/file/2UUD2UUFBYO7', //  done
    ATP_SEO: 'https://download.atpsoftware.vn/s/jDaGe6PegXRoswK/download', //,https://www.fshare.vn/file/2UUD2UUFBYO7', //  done
    CRM_PROFILE: 'https://sum.vn/new_crm_x32', //'https://www.fshare.vn/file/PP6E9SWYSTFE', // done
    SIMPLE_TIKTOK: 'http://download.atpsoftware.link/SimpleTiktok_x64.exe',
    ATP_CARE: 'https://simple.atpcare.vn/',
    SIMPLE_LIVESTREAM: 'https://sum.vn/livesteam_x32', // done nef
    ATP_SPLITTER: 'https://sum.vn/atp_spliter_x64', // done nef
    // SIMPLE_FANPAGE: 'http://uid.fanpage.vn',
    TEAM_VIEW: 'https://sum.vn/teamview_thoi',
    GIALAP: 'https://sum.vn/ldgialap',
    ULTRA_VIEW: 'https://sum.vn/ultraviewer_thoi',
    SEARCH: 'http://search.atpsoftware.vn',
    SIMPLE_PAGE: 'https://simplepage.vn/',
    SIMPLE_SHOP: 'https://simpleshop.vn/',
    ATP_LINK: 'https://atplink.com/',
    SIMPLE_MARKETPLACE: 'https://sum.vn/smp_32bit',
    FACEBOOK_ATP_SYSTEM: 'https://sum.vn/sfb_system',
    SIMPLE_NINJA_PRO: 'https://atplink.com/sp-ninja-x32'
};

export const ConstProductUrlMac = { // link mac dmg
    SIMPLE_ACCOUNT: 'https://sum.vn/simpleaccv2_mac', // done nef
    SIMPLE_FACEBOOK: 'https://sum.vn/spf_mac', // done nef
    SIMPLE_FACEBOOK_PRO: 'https://atplink.com/sp_fb_pro_macos', // done nef
    SIMPLE_TIKDOWN: 'https://atplink.com/stp_mac', // done nef
    SIMPLE_INSTAGRAM: 'https://sum.vn/sp-ins-mac', // chưa có bản mac
    SIMPLE_ZALO: 'https://atplink.com/sp_zalo_v2_macos',
    AUTOVIRAL_CONTENT: 'https://atplink.com/autoviral-content-mac', // done
    SIMPLE_ADS: 'http://download.atpsoftware.link/SimpleAds2.dmg', // done
    SIMPLE_UID: 'https://atplink.com/suid_macos', // 'https://www.fshare.vn/file/BV6VNEP3ZG2M', // done nef
    SIMPLE_SEEDING: 'https://download.atpsoftware.vn/s/ZmKQaNr7RjbRwSb/download', //'https://www.fshare.vn/file/OLM4EDELHD35', // done nef
    ATP_SEO: 'https://download.atpsoftware.vn/s/kXWxosGkEqyDx3E/download', //'https://www.fshare.vn/file/OLM4EDELHD35', // done nef
    CRM_PROFILE: 'https://sum.vn/new_crm_macos', //https://www.fshare.vn/file/D47E568CQHTG', // done
    SIMPLE_TIKTOK: 'http://download.atpsoftware.link/SimpleTiktok_x64.exe',
    ATP_CARE: 'https://atpcare.vn/simple',
    SIMPLE_LIVESTREAM: 'https://www.fshare.vn/file/TPQEGTJVKWIN', // chưa có bản mac
    ATP_SPLITTER: 'https://sum.vn/atp_spliter_x64', // done nef
    // SIMPLE_FANPAGE: 'http://uid.fanpage.vn',
    TEAM_VIEW: 'https://sum.vn/teamview_thoi',
    GIALAP: 'https://sum.vn/ldgialap',
    ULTRA_VIEW: 'https://sum.vn/ultraviewer_thoi',
    SEARCH: 'http://search.atpsoftware.vn',
    SIMPLE_PAGE: 'https://simplepage.vn/',
    SIMPLE_SHOP: 'https://simpleshop.vn/',
    ATP_LINK: 'https://atplink.com/',
    SIMPLE_MARKETPLACE: 'https://sum.vn/smp_mac',
    FACEBOOK_ATP_SYSTEM: 'https://sum.vn/sfb_system',
    SIMPLE_NINJA_PRO: 'https://atplink.com/sp-ninja-mac'
};
