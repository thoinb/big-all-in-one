export class UtilsFormatter {

    // format number  1,234,567 to 1234567
    public static formatNumberToCurrency(currency) {
        return Number(currency.replace(/,/g, ""));
    }

    // format number 1000000 to 1,234,567
    public static formatCurrencyToNumber(number) {
        return number.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }

}
