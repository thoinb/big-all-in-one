import {Md5} from 'ts-md5/dist/md5';
import {UtilsFormatter} from "./utilsFormatter";
import {MacosPlatforms} from "../enum/Platforms-Macos";
import {PlatformsWindows} from "../enum/Platforms-Windows";
import {LinuxPlatforms} from "../enum/Platforms-Linux";
import {fakeAsync} from "@angular/core/testing";
import {ConstantError} from "../constrant/ConstantError";


declare let unescape: any;

export class Utils {
    public static test: any;
    public static rowData: any = [];
    public static version: string = "1.0";
    public static visitor: any;
    public static versionKey: string;
    public static regSocket: string = 'no';
    public static interactionKey: string;
    public static isAddingFriend: boolean = false;
    public static listBtv: Array<string> = new Array<string>();
    public static isAddingFriendPost: boolean = false;
    public static finalKey: String = "";
    public static OS: String = "";
    private static jsEscapeRegex = /\\(u\{([0-9A-Fa-f]+)\}|u([0-9A-Fa-f]{4})|x([0-9A-Fa-f]{2})|([1-7][0-7]{0,2}|[0-7]{2,3})|(['"tbrnfv0\\]))|\\U([0-9A-Fa-f]{8})/g;
    private static usualEscapeSequences = {
        '0': '\0',
        'b': '\b',
        'f': '\f',
        'n': '\n',
        'r': '\r',
        't': '\t',
        'v': '\v',
        '\'': '\'',
        '"': '"',
        '\\': '\\'
    };

    public static cutStringEnd(orginal: string, startString: string, endString: string): string {
        let result = '';
        let indexStart = orginal.indexOf(startString);
        if (indexStart > -1) {
            indexStart = indexStart + startString.length;
            let indexEnd = orginal.indexOf(endString, indexStart);
            if (indexEnd > -1) {
                result = orginal.substring(indexStart, indexEnd);
            }
        }
        return result;
    }

    public static replaceAll(original: string, search: string, replacement: string): string {
        var target = original;
        return target.replace(new RegExp(search, 'g'), replacement);
    }

    public static maxNumber(listNumber: Array<number>): number {
        if (listNumber.length == 0) {
            return null;
        }
        let max;
        for (let i = 0; i < listNumber.length; i++) {
            if (i == 0) {
                max = listNumber[i];
                continue;
            }
            if (listNumber[i] > max) {
                max = listNumber[i];
            }
        }
        return max;
    }

    static getNewClientID(): string {
        let a = Date.now().toString();
        let b = Utils.g(0, 4294967295) + 1;
        a = a + ":" + b.toString();
        return encodeURIComponent(a);
    }

    public static select(m: string, fullMess: string) {
        let choices = m.split("|");
        let index = Math.floor(Math.random() * choices.length) + 0;
        fullMess = fullMess.replace('{' + m + '}', choices[index]);
        return fullMess;
    }

    static getClientID() {
        let i = Math.floor(Math.random() * 2147483648);
        return i.toString(16);
    }

    static getLoggingInteractionKey() {
        let pattern = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
        let split = pattern.split('');
        for (let i = 0; i < split.length; i++) {
            let b = Math.floor(Math.random() * 16);
            let char = split[i];
            if (char != '4' && char != '-') {
                let a;
                if (char == 'x') {
                    a = b;
                } else {
                    a = b & 3 | 8;
                }
                split[i] = a.toString(16).split('')[0];
            }
        }
        let result = '';
        for (let i = 0; i < split.length; i++) {
            result += split[i];
        }
        return result;
    }

    static escape_HTML(html_str: string) {
        'use strict';
        return html_str.replace(/[&<>"]/g, function (tag) {
            var chars_to_replace = {
                '&': '&',
                '<': '<',
                '>': '>',
                '"': '"'
            };
            return chars_to_replace[tag] || tag;
        });
    }

    public static cutStringDelete(original: string, deleteString: string) {
        let result = '';
        let index = original.indexOf(deleteString);
        if (index > -1) {
            result = original.substring(deleteString.length);
        }
        return result;
    }

    public static unescape(string) {
        return string.replace(Utils.jsEscapeRegex, (_, __, varHex, longHex, shortHex, octal, specialCharacter, python) => {
            if (varHex !== undefined) {
                return Utils.fromHex(varHex);
            } else if (longHex !== undefined) {
                return Utils.fromHex(longHex);
            } else if (shortHex !== undefined) {
                return Utils.fromHex(shortHex);
            } else if (octal !== undefined) {
                return Utils.fromOct(octal);
            } else if (python !== undefined) {
                return Utils.fromHex(python);
            } else {
                return Utils.usualEscapeSequences[specialCharacter];
            }
        });
    }

    public static getRandomIntegerInRange(min: number, max: number) {
        let randomIndex = Math.floor(Math.random() * (max - min) + min);
        return randomIndex;
    }

    public static getRandomElementInArray(array): any {
        let max = array.length;
        let min = 0;
        let randomIndex = Math.floor(Math.random() * (max - min) + min);
        let element = array[randomIndex];
        return element;
    }

    public static cutStringStartEnd(orginal: string, startString: string, endString: string): string {
        let result = '';
        let indexStart = orginal.indexOf(startString);
        if (indexStart > -1) {
            indexStart = indexStart + startString.length;
            let indexEnd = orginal.indexOf(endString, indexStart);
            if (indexEnd > -1) {
                result = orginal.substring(indexStart, indexEnd);
            }
        }
        return result;
    }

    // public static unescape(input: string): string {
    //     return unescape(input);
    // }

    /*public static unescapeHTML(input: string): string {
        var e = document.createElement('div');
        e.innerHTML = input;
        return e.childNodes[0].nodeValue;
    }*/

    public static shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    public static formatDate(date: Date): string {
        return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' - ' + date.getDate() + "/" + (date.getMonth() + 1);
    }

    public static formatDateForLogFile(date: Date): string {
        return date.getHours() + '_' + date.getMinutes() + '_' + date.getSeconds() + '_' + date.getMilliseconds() + '____' + date.getDate() + "_" + (date.getMonth() + 1);
    }

    public static buildTTStamp(fb_dtsg: string): string {
        let n = '';
        for (let o = 0; o < fb_dtsg.length; o++) {
            n += String(fb_dtsg.codePointAt(o));
        }
        n = '2' + n;
        return n;
    }

    public static onlyContainsNumber(s: string): boolean {
        return !/\D/.test(s);
    }

    public static cutString(original: string, regex: any, isFullMatch: boolean) {
        let match = original.match(regex);
        if (isFullMatch && match !== null) {
            return match[0];
        }
        if (match != null && match.length > 1) {
            return match[1];
        }
        return '';

    }

    public static getSpinT() {
        return Math.round(new Date().getTime() / 1000);
    }

    public static decode(x) {
        var r = /\\u([\d\w]{4})/gi;
        x = x.replace(r, function (match, grp) {
            return String.fromCharCode(parseInt(grp, 16));
        });
        console.log(x);  // http%3A%2F%2Fexample.com
        x = unescape(x);
        return x;
    }

    public static GetHexString(bt: string): string {
        let s: string = "";
        for (let i = 0; i < bt.length; i++) {
            let b = bt.charCodeAt(i);
            let n, n1, n2;
            n = b;
            n1 = n & 15;
            n2 = (n >> 4) & 15;
            if (n2 > 9) {
                s += (n2 - 10 + parseInt('A')).toString();
            } else {
                s += n2.toString();
            }
            if (n1 > 9) {
                s += (n1 - 10 + parseInt('A')).toString();
            } else {
                s += n1.toString();
            }
            if ((i + 1) != bt.length && (i + 1) % 2 == 0) {
                s += "-";
            }
        }
        return s;
    }

    public static getHWIDEncrypted(HWID: string): string {
        let hex = "";
        try {

            hex = Utils.GetHexString(Md5.hashStr(HWID).toString());
        } catch (e) {
            alert('Lỗi khi tạo key: ' + e.stack);
            console.log(e);
        }
        return hex;
    }

    public static getPathSimpleFacebookV1(): string {
        let pathFacebook = '';
        let KeyRegedit = (<any>window).require('windows-registry').Key;
        let windef = (<any>window).require('windows-registry').windef;
        let listPath = ['SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall', 'SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall'];
        listPath.forEach((path) => {
            try {
                let key = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path, windef.KEY_ACCESS.KEY_ALL_ACCESS);
                // key = key.openSubKey('SOFTWARE', windef.KEY_ACCESS.KEY_ALL_ACCESS);
                let listSubkeyName: Array<string> = key.getListSubKeyName();
                key.close();

                listSubkeyName.forEach((subkeyName) => {
                    if (subkeyName.length > 0) {
                        try {
                            let key = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path + '\\' + subkeyName, windef.KEY_ACCESS.KEY_ALL_ACCESS);
                            let displayName = key.getValue('DisplayName');
                            if (displayName !== undefined && displayName.indexOf('Simple Livestream') > -1) {
                                pathFacebook = key.getValue('InstallLocation');
                                return;
                            }
                            key.close();
                        } catch (ee) {
                            console.log(ee, subkeyName);
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
            if (pathFacebook.length > 0) {
                return;
            }
        });
        return pathFacebook;
    }

    // public static log(message: string) {
    //     LogControl.getInstance().insertLog(message);
    // }

    static execute(command) {
        return new Promise<any>((resolve, reject) => {
            setTimeout(() => {
                let exec = (<any>window).require('child_process').exec;
                exec(command, function (error, stdout, stderr) {
                    resolve(stdout);
                });
            }, 1000);
        });
    };

    public static getNumericValue(a) {
        let c = 0;
        for (var d = 0; d < a.length; d++)
            c += a.charCodeAt(d);

        return '2' + c.toString();
    }

    private static fromHex = (str) => String.fromCodePoint(parseInt(str, 16));

    private static fromOct = (str) => String.fromCodePoint(parseInt(str, 8));

    private static g(a: number, b: number) {
        return Math.floor(a + Math.random() * (b - a));
    }

    private static pasteTextFromClipboard() {

    }

    public static bestCopyEver(src) {
        return Object.assign({}, src);
    }

    public static cloneArray(arrOut, arrIn) {
        arrOut = new Array();
        return arrOut.concat(arrIn);
    }

    //https://codepen.io/559wade/pen/LRzEjj
    public static formatCurrency(input) {
        let input_val = input.val();

        let numberInputVal = UtilsFormatter.formatNumberToCurrency(input_val);

        let original_len = input_val.length;

        let caret_pos = input.prop("selectionStart");

        input_val = UtilsFormatter.formatCurrencyToNumber(numberInputVal);
        input.val(input_val);

        // put caret back in the right position
        let updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);

        return numberInputVal;
    }

    /**
     * Xử lý kết quả trả về từ server trả về
     * Processing results returned from the server
     * @param result
     * @param getDataJson
     * @return null || getDataJson
     */
    public static resultFromRequest(result, getDataJson) {
        if (result !== "") {
            getDataJson = JSON.parse(result);
            if (getDataJson.status && getDataJson.error || getDataJson.path || getDataJson.message || getDataJson.timestamp) {
                let error = getDataJson.status + "\n \ "
                    + getDataJson.error + "\n \ "
                    + getDataJson.path + "\n \ "
                    + getDataJson.message + "\n \ "
                    + getDataJson.timestamp + "\n \ ";
                throw new Error(error);
            } else {
                return getDataJson;
            }
        }
        return null;
    }

    public static getOS() {
        let platform = window.navigator.platform,
            macosPlatforms = [MacosPlatforms.MAC_INTOSH, MacosPlatforms.MAC_INTEL, MacosPlatforms.MAC_PPC, MacosPlatforms.MAC_68K],
            windowsPlatforms = [PlatformsWindows.WINDOWS_32, PlatformsWindows.WINDOWS_64, PlatformsWindows.WINDOWS, PlatformsWindows.WIN_CE ]

        if (macosPlatforms.indexOf(<MacosPlatforms>platform) !== -1) {
            return MacosPlatforms.MAC_OS;
        } else if (windowsPlatforms.indexOf(<PlatformsWindows>platform) !== -1) {
            if (platform === PlatformsWindows.WINDOWS_32) {
                return PlatformsWindows.WINDOWS_32
            } else if (platform === PlatformsWindows.WINDOWS_64) {
                return PlatformsWindows.WINDOWS_64
            }
            return PlatformsWindows.WINDOWS
        } else if (/Linux/.test(platform)) {
            return false
        } else {
            return false;
        }
    }


    public static messageDis() {
        alert("Bạn chưa cập nhật hoặc cập thiếu thông tin ở tab bản quyền. Vùi lòng điển đủ thông tin để sử dụng phần mềm.");
        return false;
    }

    public static HTMLEncode(str: string) {
        let i = str.length,
            aRet = [];
    
        while (i--) {
            let iC = str.charCodeAt(i);
            if (iC < 65 || iC > 127 || (iC > 90 && iC < 97)) {
                aRet[i] = '&#' + iC + ';';
            } else {
                aRet[i] = str[i];
            }
        }
        return aRet.join('');
    }

    public static getIDLinkYoutube(link: string){
        let splitLink= link.split("/");
        return splitLink;
    }

    public static decodeHTMLEntities(str) {
        let element = document.createElement('div');
        if (str && typeof str === 'string') {
          // strip script/html tags
          str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
          str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
          element.innerHTML = str;
          str = element.textContent;
          element.textContent = '';
        }
        return str;
      }

    public static key: string = "";
    
    public static async getKey() {
        if (Utils.key === "") {
            let path = (<any>window).require('path');
            let pathAsar = path.join(__dirname, '../../', 'app');
            let keyHex = await Utils.execute('"' + pathAsar + '\\info.exe" simple-facebook');
            if (keyHex !== undefined) {
                keyHex = keyHex.replace(/(\r\n|\n|\r)/gm, "");
                keyHex = keyHex.trim();
            }
            Utils.key = 'ATP-BIG-' + keyHex;
        };
        return Utils.key;
    };

    /**
     * showError
     */
    public static showError(func, err) {
        let error = null;
        try {
            error = err.stack
        } catch (error) {
            error = err
        }
        alert(ConstantError.LOGGER_ERROR_ATP + '\n' + func + '\n' + error);
    }


    public static getError(e) {
        let error = '';
        if (e.message) {
            error += e.message + '\n';
        }
        if (e.stack) {
            error += e.stack + '\n';
        }
        try {
            error += JSON.stringify(e) + '\n';
        } catch (e) {
            error += e.toString();
        }
        return error;
    }


    public static changeAlias(alias) {
        let str = alias;
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        return str;
    }

}
