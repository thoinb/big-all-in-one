import {HTTPResponse} from './http-response';
import {IpcRenderer} from "electron";

export class HTTP {
    private ipc: IpcRenderer;

    constructor() {
        if ((<any>window).require) {
            try {
                this.ipc = (<any>window).require("electron").ipcRenderer;
            } catch (error) {
                throw error;
            }
        } else {
            console.warn("Could not load electron ipc");
        }
    }

    public async get(url: string, params: Object, headers: Object): Promise<HTTPResponse> {
        let idRender = this.makeid();
        return new Promise<HTTPResponse>((resolve, reject) => {
            this.ipc.once("getRequestResponseSuccess" + idRender, (event, arg) => {
                resolve(arg);
                this.ipc.removeAllListeners("getRequestResponseError" + idRender);
            });

            this.ipc.once("getRequestResponseError" + idRender, (event, arg) => {
                reject(arg);
                this.ipc.removeAllListeners("getRequestResponseSuccess" + idRender);
            });
            this.ipc.send("getRequest", {url: url, params: params, headers: headers, keyRender: idRender});
        });
    }

    public async post(url: string, params: Object, headers: Object): Promise<HTTPResponse> {
        let query = this.buildQueryString(params);
        let length = query.length;
        let idRender = this.makeid();
        headers["Content-Type"] = "application/x-www-form-urlencoded";
        headers["Content-Length"] = length;
        return new Promise<HTTPResponse>((resolve, reject) => {
            this.ipc.once("postRequestResponseSuccess" + idRender, (event, arg) => {
                resolve(arg);
                this.ipc.removeAllListeners("postRequestResponseError" + idRender);
            });

            this.ipc.once("postRequestResponseError" + idRender, (event, arg) => {
                reject(arg);
                this.ipc.removeAllListeners("postRequestResponseSuccess" + idRender);
            });
            this.ipc.send("postRequest", {url: url, params: query, headers: headers, keyRender: idRender});
        });
    }

    private buildQueryString(params): String {
        let esc = encodeURIComponent;
        let query = Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');
        return query;
    }

    private makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
}
