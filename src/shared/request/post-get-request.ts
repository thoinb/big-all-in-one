import {HTTP} from "./http";
import {HTTPResponse} from "./http-response";
import {Constant} from "../constrant/Constant";
import {Utils} from "../utils/utils";
import {RequestControl} from "../../controller/request-control";

export class PostGetRequest {
    public url: string = "";
    public taskName = "";
    public result: string = "";
    public cookie: string = "";
    public user_agent: string = "";
    public body: Map<string, string> = new Map<string, string>();
    public origin: string = 'https://www.facebook.com';
    public accept_language: string = "en-US,en;q=0.8";
    public headerRespon = {};
    public redirectUrl: string = '';
    public noSleep: Boolean = false;


    public headerPost = {};
    public headerGet = {};
    public headerDownload = {};


    public async task() {
    }

    public async sendGet(params?: object): Promise<HTTPResponse> {
        if (!params) {
            params = {};
        }
        let http: HTTP = new HTTP();
        if (this.cookie != '') {
            this.headerGet['Cookie'] = this.cookie;
        }

        if (this.user_agent == null || this.user_agent.length == 0) {
            this.headerGet['User-Agent'] = Constant.user_agent;
        } else {
            this.headerGet['User-Agent'] = this.user_agent;
        }
        let result = await http.get(this.url, params, this.headerGet);
        // console.log(result);
        this.result = result.data;
        if (this.url.includes('facebook.com')) {
            if (this.noSleep == false) {
                await this.sleep(Utils.getRandomIntegerInRange(200, 1000));
            }
        }
        return result;
    }

    public async sendGetAccessTokenWithOutMessage() {
        let http: HTTP = new HTTP();
        if (this.user_agent == null || this.user_agent.length == 0) {
            this.headerGet['User-Agent'] = Constant.user_agent;
        } else {
            this.headerGet['User-Agent'] = this.user_agent;
        }
        if (this.cookie != '') {
            this.headerGet['Cookie'] = this.cookie;
        }
        let result;
        try {
            result = await http.get(this.url, {}, this.headerGet);
            this.result = result.data;
        } catch (e) {
            this.result = e.error;
        }
        if (this.url.includes('facebook.com')) {
            if (this.noSleep == false) {
                await this.sleep(Utils.getRandomIntegerInRange(200, 1000));
                await RequestControl.getInstance().submitRequestAndSleepIfNeeded();
            }
        }
    }

    public async sendPost(params: object) {
        let http: HTTP = new HTTP();
        if (this.user_agent == null || this.user_agent.length == 0) {
            this.headerPost['User-Agent'] = Constant.user_agent;
        } else {
            this.headerPost['User-Agent'] = this.user_agent;
        }
        if ("" != this.cookie) {
            // console.log(this.cookie);
            this.headerPost['Cookie'] = this.cookie;
        }
        let result = await http.post(this.url, params, this.headerPost);
        this.result = result.data;
        // this.headerRespon = result.headers;
        if (this.url.includes('facebook.com')) {
            if (this.noSleep == false) {
                await this.sleep(Utils.getRandomIntegerInRange(200, 1000));
                await RequestControl.getInstance().submitRequestAndSleepIfNeeded();
            }
        }
        return result;
    }

    public getResult(): string {
        return this.result;
    }

    public async sleep(ms: number) {
        await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log('sleep for: ' + ms));
    }

}
