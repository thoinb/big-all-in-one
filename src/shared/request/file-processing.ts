import {IpcRenderer} from "electron";
import {HTTPResponse} from "./http-response";
import {Registry} from "../constrant/Registry";
import {ProductStatus} from "../enum/ProductStatus";
import {TypeTool} from "../enum/TypeTool";
import {PostInfoToServer} from "../../request/POST/post-info-to-server";
import {ConstProductKey} from "../const/ConstProduct";

export class FileProcessing {
    private ipc: IpcRenderer;
    //private pathSave: string = process.execPath.substring(0, process.execPath.lastIndexOf("big-all-in-one")) + 'big-all-in-one\\';

    private pathSave: string =  process.resourcesPath.substring(0, process.resourcesPath.lastIndexOf("resources"));

    constructor() {
        if ((<any>window).require) {
            try {
                this.ipc = (<any>window).require("electron").ipcRenderer;
            } catch (error) {
                throw error;
            }
        } else {
            console.warn("Could not load electron ipc");
        }
    }

    public checkStatusSofeware(lstMapProductRender, onResult) {
        for (let key of Array.from(lstMapProductRender.keys())) {
            if (lstMapProductRender.get(key).typeTool === TypeTool.WEB) {
                continue;
            }

            this.checkStatusSofewareFileInstall(lstMapProductRender.get(key), (productRender) => {
                onResult(productRender);
            });
        }

        this.checkStatusSoftwareHasBeenInstalled(lstMapProductRender, (productRender) => {
            onResult(productRender);
        });
    }

    public checkStatusSofewareFileInstall(productRender, onResult) {

        let targetPath = this.pathSave + productRender.file_name;

        if ((<any>window).require('fs').existsSync(targetPath)) {
            productRender.file_install = targetPath;
            productRender.status = ProductStatus.DOWNLOADED_FILE_INSTALL; // đã có file cài đặt rồi
            if (productRender.productKey === ConstProductKey.ATP_SPLITTER) {
                productRender.file_open = targetPath;
                productRender.status = ProductStatus.HAS_BEEN_FILE_INSTALLED; // đã có file cài đặt rồi
            }
            onResult(productRender);
        } else {
            productRender.status = ProductStatus.NOT_FILE_INSTALL; // chưa có file cài đặt
            onResult(productRender);
        }
    }

    public async checkStatusSoftwareHasBeenInstalled(lstMapProductRender, onResult) {
        let KeyRegedit = (<any>window).require('windows-registry').Key;
        let windef = (<any>window).require('windows-registry').windef;
        if ((<any>window).process.platform === 'win32') {
            try {
                let key = new KeyRegedit(windef.HKEY.HKEY_CURRENT_USER, 'Software\\ATP', windef.KEY_ACCESS.KEY_ALL_ACCESS);
            } catch (e) {
                let key = new KeyRegedit(windef.HKEY.HKEY_CURRENT_USER, 'Software', windef.KEY_ACCESS.KEY_ALL_ACCESS);
                key.createSubKey('\ATP', windef.KEY_ACCESS.KEY_ALL_ACCESS);
                key.close();
                key = new KeyRegedit(windef.HKEY.HKEY_CURRENT_USER, 'Software\\ATP', windef.KEY_ACCESS.KEY_ALL_ACCESS);
            }
        }

        let path = 'SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall';
        let key = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path, windef.KEY_ACCESS.KEY_ALL_ACCESS);
        let listSubkeyName: Array<string> = key.getListSubKeyName();
        key.close();
        listSubkeyName.forEach((subkeyName) => {

            if (subkeyName.length > 0) {
                try {

                    let regeditKey = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path + '\\' + subkeyName, windef.KEY_ACCESS.KEY_ALL_ACCESS);

                    if (subkeyName === 'TeamViewer') {
                        lstMapProductRender.get(ConstProductKey.TEAM_VIEW).file_open = regeditKey.getValue('DisplayIcon');
                        lstMapProductRender.get(ConstProductKey.TEAM_VIEW).status = ProductStatus.HAS_BEEN_FILE_INSTALLED;
                    }

                    if (subkeyName === 'LDPlayer') {
                        lstMapProductRender.get(ConstProductKey.GIALAP).file_open = regeditKey.getValue('DisplayIcon');
                        lstMapProductRender.get(ConstProductKey.GIALAP).status = ProductStatus.HAS_BEEN_FILE_INSTALLED;
                    }

                    for (let key of Array.from(lstMapProductRender.keys())) {
                        let product = lstMapProductRender.get(key);
                        if (product.typeTool === TypeTool.WEB) {
                            continue;
                        }

                        if (product.productKey === ConstProductKey.ATP_SPLITTER) {
                            continue;
                        }

                        if (product.status === ProductStatus.HAS_BEEN_FILE_INSTALLED) {
                            continue;
                        }
                        let installLocation = regeditKey.getValue('InstallLocation') ? regeditKey.getValue('InstallLocation') : "";
                        let innoSetupAppPath = regeditKey.getValue('Inno Setup: App Path') ? regeditKey.getValue('Inno Setup: App Path') : "";
                        let displayName = regeditKey.getValue('DisplayName') ? regeditKey.getValue('DisplayName') : "";
                        let quietUninstallString = regeditKey.getValue('QuietUninstallString') ? regeditKey.getValue('DisplayName') : "";
                        let uninstallString = regeditKey.getValue('UninstallString') ? regeditKey.getValue('UninstallString') : "";

                        let valuesCheck = displayName + quietUninstallString + uninstallString + installLocation;
                        let check = valuesCheck.includes(product.productName);
                        if (check) {
                            product.file_open = innoSetupAppPath + '\\' + product.productName;
                            if (product.productKey === ConstProductKey.ULTRA_VIEW) {
                                product.file_open = innoSetupAppPath + '\\' + product.name2;
                            }
                            product.status = ProductStatus.HAS_BEEN_FILE_INSTALLED;
                            console.log('PHAN MEM DA CAI DAT |' + product.productName);
                            onResult(product);
                        }
                    }
                    regeditKey.close();
                } catch (ee) {
                    console.warn(ee, subkeyName);
                }
            }
        });

        path = 'SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall';
        key = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path, windef.KEY_ACCESS.KEY_ALL_ACCESS);

        listSubkeyName = key.getListSubKeyName();
        key.close();

        listSubkeyName.forEach((subkeyName) => {
            if (subkeyName.length > 0) {
                try {
                    let regeditKey = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path + '\\' + subkeyName, windef.KEY_ACCESS.KEY_ALL_ACCESS);
                    // let displayName = key.getValue('DisplayName');

                    for (let key of Array.from(lstMapProductRender.keys())) {

                        let product = lstMapProductRender.get(key);
                        if (product.typeTool === TypeTool.WEB) {
                            continue;
                        }

                        if (product.productKey === ConstProductKey.ATP_SPLITTER) {
                            continue;
                        }

                        if (product.status === ProductStatus.HAS_BEEN_FILE_INSTALLED) {
                            continue;
                        }

                        let displayIcon = regeditKey.getValue('DisplayIcon') ? regeditKey.getValue('DisplayIcon') : '' ;
                        let displayName = regeditKey.getValue('DisplayName')? regeditKey.getValue('DisplayName') : '' ;
                        let quietUninstallString = regeditKey.getValue('QuietUninstallString') ? regeditKey.getValue('QuietUninstallString') : '' ;
                        let uninstallString = regeditKey.getValue('UninstallString') ? regeditKey.getValue('UninstallString') : '' ;
                        let valuesCheck = displayIcon + displayName + quietUninstallString + uninstallString;
                        if (lstMapProductRender.get(key).productKey === ConstProductKey.SIMPLE_SEEDING) {
                            product.productName = 'ATP Seeding';
                        }
                        let check = valuesCheck.includes(product.productName);
                        if (check) {
                            if (displayIcon) {
                                let index = displayIcon.indexOf(',0');
                                product.file_open = displayIcon.substring(0, index);
                            }

                            product.status = ProductStatus.HAS_BEEN_FILE_INSTALLED;
                            console.log('PHAN MEM DA CAI DAT |' + product.productName);
                            onResult(product);
                        }
                    }
                    regeditKey.close();
                } catch (ee) {
                    console.warn(ee, subkeyName);
                }
            }
        });

        /*path = 'SOFTWARE';
        key = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path, windef.KEY_ACCESS.KEY_ALL_ACCESS);
        listSubkeyName = key.getListSubKeyName();
        key.close();

        listSubkeyName.forEach((subkeyName) => {
            if (subkeyName.length > 0) {
                try {
                    let regeditKey = new KeyRegedit(windef.HKEY.HKEY_LOCAL_MACHINE, path + '\\' + subkeyName, windef.KEY_ACCESS.KEY_ALL_ACCESS);

                    for (let key of Array.from(lstMapProductRender.keys())) {


                        if (lstMapProductRender.get(key).status === ProductStatus.HAS_BEEN_FILE_INSTALLED) {
                            continue;
                        }
                        if (lstMapProductRender.get(key).typeTool === TypeTool.WEB) {
                            continue;
                        }
                        if (lstMapProductRender.get(key).productKey === ConstProductKey.ATP_SPLITTER) {
                            continue;
                        }

                        if (lstMapProductRender.get(key).productKey === ConstProductKey.TEAM_VIEW) {
                            continue;
                        }
                        let productName = lstMapProductRender.get(key).productName;
                        let installLocation = regeditKey.getValue('InstallLocation');
                        let shortcutName = regeditKey.getValue('ShortcutName');
                        let valuesCheck = installLocation + shortcutName;
                        if (lstMapProductRender.get(key).productKey === ConstProductKey.SIMPLE_SEEDING) {
                            productName = 'ATP Seeding';
                        }
                        let check = valuesCheck.includes(productName);
                        if (check) {
                            lstMapProductRender.get(key).file_open = installLocation+'/'+lstMapProductRender.get(key).name2;
                            lstMapProductRender.get(key).status = ProductStatus.HAS_BEEN_FILE_INSTALLED;
                            onResult(lstMapProductRender.get(key));
                        }
                    }
                    regeditKey.close();
                } catch (ee) {
                    console.log(ee, subkeyName);
                }
            }
        });*/
    }

    public downloadFile(product, onProgress, onDone, onError) {

        let userAgent = navigator.userAgent;
        let url = product.file_url_64;
        if (userAgent) {
            if (userAgent.indexOf('Mac') > -1) {
                url = product.file_url_mac;
            } else if (userAgent.indexOf("WOW64") != -1 || userAgent.indexOf("Win64") != -1 ){
                url = product.file_url_64;
            } else {
                url = product.file_url_32
            }
        }

        this.ipc.on("downloadProgress" + product.productKey, (event, productKey, progress) => {
            onProgress(productKey, progress);
        });

        this.ipc.once("downloadComplete" + product.productKey, (event, productKey) => {
            onDone(productKey, this.pathSave);
            this.removeAllListeners(productKey);
        });

        this.ipc.once("downloadError" + product.productKey, (event, productKey, error) => {
            onError(productKey, error);
            this.removeAllListeners(productKey);
        });

        this.ipc.send("download", {
            url: url,
            file_name: product.file_name
        }, product, userAgent);

    }

    public cancelDownloadFile(product, onDone, onError) {
        return new Promise<HTTPResponse>((resolve, reject) => {
            this.ipc.once("cancelDownloadComplete" + product.productKey, (event, productKey) => {
                onDone(productKey);
                this.removeAllListeners(productKey);
            });

            this.ipc.once("cancelDownloadError" + product.productKey, (event, productKey, error) => {
                onError(productKey, error);
                this.removeAllListeners(productKey);
            });

            this.ipc.send("cancelDownload", product.productKey);
        });
    }

    public openFile(url) {
        (<any>window).require('child_process').exec('"' + url + '"', {
            cwd: url.substring(0, url.lastIndexOf('\\'))
        }, function (error, stdout, stderr) {
            if (error) {
                console.log(error);
            }
            console.log(error);
            console.log(stdout);
            console.log(stderr);
        });
    }

    private removeAllListeners(productKey) {
        this.ipc.removeAllListeners("downloadProgress" + productKey);
        this.ipc.removeAllListeners("downloadComplete" + productKey);

        this.ipc.removeAllListeners("cancelDownloadComplete" + productKey);
        this.ipc.removeAllListeners("cancelDownloadError" + productKey);

        this.ipc.removeAllListeners("openFileError" + productKey);

        this.ipc.removeAllListeners("checkStatusSofewareError");
        this.ipc.removeAllListeners("checkStatusSofewareResult" + productKey);
    }

    public async getKeyRegistry(): Promise<any> {
        return new Promise( (resolve,reject) => {
            (<any>window).require('regedit').list([Registry.HKCU_Software_ATP], function (err, data) {
                if (err) {
                    console.log('err' + err);
                    reject(err);
                }
                resolve(data ? data["HKCU\\Software\\ATP\\"] ? data["HKCU\\Software\\ATP\\"].values : null : null);
            });
        });
    }

    public getInfoLicense(onDone) {
        (<any>window).require('regedit').list("HKCU\\Software\\ATP", async function (err, result) {
            onDone(result);
        });
    }

    public async saveRegeditATP(name, phone, note, email, key, gender) {
       await (<any>window).require('regedit').putValue({
            'HKCU\\Software\\ATP': {
                nameATP: {
                    value: name.toString(),
                    type: 'REG_SZ'
                },
                phoneATP: {
                    value: phone.toString(),
                    type: 'REG_SZ'
                },
                noteATP: {
                    value: note ? note.toString() : null,
                    type: 'REG_SZ'
                },
                emailATP: {
                    value: email.toString(),
                    type: 'REG_SZ'
                },
                genderATP: {
                    value: gender,
                    type: 'REG_SZ'
                }
            },
        }, function (err) {
            console.log(err)
        });


        let param = {};
        param['nameATP'] = name;
        param['emailATP'] = email;
        param['phoneATP'] = phone;
        param['genderATP'] = gender === "0" ? 'Nữ' : 'Nam';
        param['key'] = email;
        let postInfoToServer = new PostInfoToServer(param);
        await postInfoToServer.task();
    }

}
