import {app, BrowserWindow, ipcMain, Menu, net, screen} from "electron";
import {download} from "electron-dl";
import * as request from "request";
import * as url from "url";
import * as fs from "fs";
import * as path from "path";

let pathSave = process.resourcesPath.substring(0, process.resourcesPath.lastIndexOf("resources"));
// let pathSave = process.execPath.substring(0, process.execPath.lastIndexOf("big-all-in-one")) + 'big-all-in-one\\';
let arrProductMain = new Map();

let win: BrowserWindow;
let version = '2.4';

app.on("ready", createWindow);

app.on("activate", () => {
    if (win === null) {
        createWindow()
    }
});

function createWindow() {
    win = new BrowserWindow({width: 1368, height: 740, minWidth: 1025, minHeight: 740, center: true});
    /*win.setResizable(false);*/
    win.setMenuBarVisibility(false); // Remove menubar from Electron app
    win.webContents.on('will-navigate', ev => { // khi bấm vào các thẻ <a href="#"></a> sẽ ko bị chuyển hướng sang trang trắng
        ev.preventDefault()
    });
    win['version'] = version;
    win['nameWindows'] = {name: 'mainWindows'};
    win.webContents.clearHistory();
    win.webContents.session.clearCache((aaa) => {
        console.log(aaa);
    });
    win.loadURL(`https://crm.alosoft.vn/bigallinone_05_05_2020/index.html`);
    // win.loadURL(
    //     url.format({
    //         pathname: path.join(__dirname, `/../../dist/angular-electron/index.html`),
    //         protocol: "file:",
    //         slashes: true
    //     })
    // );
    try {
        const {setup: setupPushReceiver} = require('electron-push-receiver');
        setupPushReceiver(win.webContents);
    } catch (e) {
        console.log(e);
    }
    var template: any = [{
        label: "Application",
        submenu: [
            {label: "About Application", selector: "orderFrontStandardAboutPanel:"},
            {type: "separator"},
            {
                label: "Quit", accelerator: "Command+Q", click: function () {
                    app.quit();
                }
            }
        ]
    }, {
        label: "Edit",
        submenu: [
            {label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:"},
            {label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:"},
            {type: "separator"},
            {label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:"},
            {label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:"},
            {label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:"},
            {label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:"}
        ]
    }, {
        label: "View",
        submenu: [
            {
                label: "DevTool", accelerator: "CmdOrCtrl+Shift+I", click: function () {
                    win.webContents.openDevTools();
                }
            }
        ]
    }
    ];

    Menu.setApplicationMenu(Menu.buildFromTemplate(template));
    win.on("closed", () => {
        app.quit();
        win = null;
    });
}

ipcMain.on("getRequest", (event, arg) => {
    const request = net.request(arg.url);
    let headers = arg.headers;
    let idRender = arg.keyRender;
    Object.keys(headers).forEach(function (key) {
        request.setHeader(key, headers[key]);
    });

    request.on('error', (error) => {
        console.log(error);
        try {
            event.sender.send("getRequestResponseError" + idRender, error !== undefined ? error.message : '');
        } catch (e) {
            console.log(e);
        }
    });

    request.on('response', (response) => {
        let data = "";
        response.on('data', (chunk) => {
            data += chunk;
        });
        response.on('end', () => {
            try {
                response['data'] = data;
                event.sender.send("getRequestResponseSuccess" + idRender, response);
            } catch (e) {
                console.log(e);
            }
        })
    });
    request.end();
});

ipcMain.on("postRequest", (event, arg) => {
    const request = net.request({method: 'POST', url: arg.url});
    let headers = arg.headers;
    let idRender = arg.keyRender;
    let postData = arg.params;
    Object.keys(headers).forEach(function (key) {
        request.setHeader(key, headers[key]);
    });
    request.on('error', (error) => {
        try {
            event.sender.send("postRequestResponseError" + idRender, error !== undefined ? error.message : '');
        } catch (e) {

        }
    });

    request.on('response', (response) => {
        let data = "";
        response.on('data', (chunk) => {
            data += chunk;
        });
        response.on('end', () => {
            try {
                response['data'] = data;
                event.sender.send("postRequestResponseSuccess" + idRender, response);
            } catch (e) {
                console.log(e);
            }
        })
    });
    request.write(postData);
    request.end();
});

ipcMain.on("getFiles", () => {
    console.log("C:\\");
    const files = fs.readdirSync("C:\\");
    win.webContents.send("getFilesResponse", files);
});

function addArrProductMain(productKey, productNAme, request, file_url, targetPath, status) {
    arrProductMain.set(productKey, {
        productKey: productKey,
        productName: productNAme,
        request: request,
        file_url: file_url,
        targetPath: targetPath,
        status: status
    })

    console.log(targetPath);
}

ipcMain.on("download", (event, file, productKey, userAgent) => {
    try {
        downloadFile(file.url, pathSave + file.file_name, event, productKey, userAgent);
    } catch (error) {
        event.sender.send("downloadError" + productKey, productKey, error);
    }
});

function downloadFile(file_url, targetPath, event, product, userAgent) {
    let received_bytes = 0;
    let total_bytes = 0;
    let req;
    // let isSum = false;

    if (file_url.indexOf("download.atpsoftware") !== -1) {
        console.log("-1 " + file_url);
        req = request({
            method: 'GET',
            rejectUnauthorized: false,
            uri: file_url,
        });
        addArrProductMain(product.productKey, product.productName, req, file_url, targetPath, 1);
        let out = fs.createWriteStream(targetPath);
        req.pipe(out);
        req.on('response', function (data) {
            if (data.statusCode === 404) {
                let productMainToCancel = arrProductMain.get(product.productKey);
                stopRequest(event, productMainToCancel);
                deleteFile(event, productMainToCancel);
                return event.sender.send("downloadError" + product.productKey, product.productKey, "URL not found :" + file_url);
            }

            total_bytes = parseInt(data.headers['content-length']);
            if (!total_bytes) {
                total_bytes = 0;
            }

            req.on('data', function (chunk) {
                // console.log(chunk);
                received_bytes += chunk.length;
                console.log(received_bytes);
                showProgress(received_bytes, total_bytes, event, product.productKey);
            });
            req.on('end', function () {
                showDone(event, product.productKey);
            });
        });
    } else {
        console.log("0 " + file_url);
        req = request({
            method: 'GET',
            rejectUnauthorized: false,
            headers: { 'User-Agent': userAgent },
            uri: file_url + '?utm_all=atp',
            followAllRedirects: true
        });

        addArrProductMain(product.productKey, product.productName, req, file_url, targetPath, 1);
        req.on('response', function (data) {
            let setCookie = data.headers['set-cookie'];
            if (setCookie) {
                req = request({
                    method: 'GET',
                    rejectUnauthorized: false,
                    headers: { 'User-Agent': userAgent, 'cookie': getCookie(setCookie)},
                    uri: file_url + '?utm_all=atp',
                    followAllRedirects: true
                });

                req.on('response', function (data) {
                    let out = fs.createWriteStream(targetPath);
                    req.pipe(out);

                    if (data.statusCode === 404) {
                        let productMainToCancel = arrProductMain.get(product.productKey);
                        stopRequest(event, productMainToCancel);
                        deleteFile(event, productMainToCancel);
                        return event.sender.send("downloadError" + product.productKey, product.productKey, "URL not found :" + file_url);
                    }
                    total_bytes = parseInt(data.headers['content-length']);
                    if (!total_bytes) {
                        total_bytes = 0;
                    }

                    req.on('data', function (chunk) {
                        // console.log(chunk);
                        received_bytes += chunk.length;
                        showProgress(received_bytes, total_bytes, event, product.productKey);
                    });
                    req.on('end', function () {
                        showDone(event, product.productKey);
                    });
                });
            } else {
                let out = fs.createWriteStream(targetPath);
                req.pipe(out);
                if (data.statusCode === 404) {
                    let productMainToCancel = arrProductMain.get(product.productKey);
                    stopRequest(event, productMainToCancel);
                    deleteFile(event, productMainToCancel);
                    return event.sender.send("downloadError" + product.productKey, product.productKey, "URL not found :" + file_url);
                }
                total_bytes = parseInt(data.headers['content-length']);
                if (!total_bytes) {
                    total_bytes = 0;
                }
                req.on('data', function (chunk) {
                    // console.log(chunk);
                    received_bytes += chunk.length;
                    console.log(received_bytes);
                    showProgress(received_bytes, total_bytes, event, product.productKey);
                });
                req.on('end', function () {
                    showDone(event, product.productKey);
                });
            }
        });
    }

    //let cookie = getCookie(data.headers['set-cookie']);
    //console.log(cookie)
}

function getCookie(setCookie) {
    let a = setCookie[0];
    let b = setCookie[1];
    let c = setCookie[2];
    let indexOfa = a.indexOf(';');
    let indexOfb = b.indexOf(';');
    let indexOfc = c.indexOf(';');
    return a.substring(0, indexOfa) + ';' + b.substring(0, indexOfb) + ';' + c.substring(0, indexOfc);
    ;
}

function showProgress(received, total, event, productKey) {
    let percentage = (received * 100) / total;
    event.sender.send("downloadProgress" + productKey, productKey, percentage);
}

function showDone(event, productKey) {
    arrProductMain.get(productKey).status = 2; // đã có file cài đặt
    event.sender.send("downloadComplete" + productKey, productKey, arrProductMain.get(productKey));
}

ipcMain.on("cancelDownload", (event, productKey) => {
    try {
        cancelDownloadFile(event, productKey);
    } catch (err) {
        event.sender.send("cancelDownloadError" + productKey, productKey, err);
    }
});

function cancelDownloadFile(event, productKey) {
    // không sữ lý then vì đã xử lý trên giao diện
    if (arrProductMain.size > 0) {
        let downloadToCancel = arrProductMain.get(productKey);
        if (downloadToCancel.status === 1) { // đang tải file cài đặt
            stopRequest(event, downloadToCancel);
            deleteFile(event, downloadToCancel);
            event.sender.send("cancelDownloadComplete" + productKey, productKey).then(r => {
                arrProductMain.get(productKey).status = 0; // chưa tải file cài đặt
            });
        }
    }
}

function stopRequest(event, urlDownloading) {
    urlDownloading.request.abort();
}

function deleteFile(event, urlDownloading) {
    console.log(urlDownloading.targetPath);
    fs.unlink(urlDownloading.targetPath, (err) => {
        if (err) {
            urlDownloading.status = 0; // chưa tải file cài đặt
            event.sender.send("cancelDownloadError" + urlDownloading.productKey, urlDownloading.productKey, err);
        }
    });
}


/*
noti
* */

let mapDataNoti = new Map();
let mapIntanceWindowsNoti = new Map();

ipcMain.on('showNoti', (event, dataNoti) => {
    let id = dataNoti.idCampaign;
    if (mapIntanceWindowsNoti.get(id)) {
        mapIntanceWindowsNoti.get(id).show();
        return;
    }
    mapDataNoti.set(id, dataNoti);
    let instanceWindowsNoti = showNotification(dataNoti);
    mapIntanceWindowsNoti.set(id, instanceWindowsNoti);
})
ipcMain.on('closeNoti', (event, dataNoti) => {
    let idCampaign = dataNoti.idCampaign;
    let windowsNoti: BrowserWindow = mapIntanceWindowsNoti.get(idCampaign);
    if (windowsNoti) {
        windowsNoti.close();
        windowsNoti = null;
        mapDataNoti.delete(idCampaign);
        mapIntanceWindowsNoti.delete(idCampaign);
    }
});

ipcMain.on('clickNoti', (event, dataNoti) => {
    win.webContents.send("clickedNoti", dataNoti);
});

function showNotification(data) {
    let sizeScreen = getSizeScreen();
    const widthScreen = sizeScreen[0];
    const heightScreen = sizeScreen[1];
    let width = 400;
    let height = 220;
    let windowsNoti = new BrowserWindow({
        width: width,
        maxHeight: 500,
        height: 187,
        maximizable: false,
        hasShadow: true,
        alwaysOnTop: true,
        resizable: false,
        frame: false,
        transparent: true,
        x: widthScreen - width,
        y: heightScreen - height + 10,
        webPreferences: {
            webSecurity: false,
            nodeIntegration: true
        }
    });
    let urlLoad = "http://crm.alosoft.vn/notiView/index.html";
    windowsNoti.webContents['dataNoti'] = data;
    windowsNoti.loadURL(urlLoad);
    //windowsNoti.webContents.openDevTools();
    return windowsNoti;
}

function getSizeScreen() {
    const {width, height} = screen.getPrimaryDisplay().workAreaSize
    return [width, height];
}
